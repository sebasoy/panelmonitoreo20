<!doctype html>
<html>

<head>
	<meta charset="utf-8">
	<title>Bienvenid@ a Smartsen Monitoreo Energético</title>

	<!-- FONT GOOGLE -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700|Supermercado+One" rel="stylesheet">

	<!-- CSS -->
	<link href="css/MarcosCSS.css" rel="stylesheet" type="text/css" />
	<link href="css/MenuCSS.css" rel="stylesheet" type="text/css" />
	<link href="css/LogCSS.css" rel="stylesheet" type="text/css" />
	<link href="css/Header-UserCSS.css" rel="stylesheet" type="text/css" />
	<link href="css/ConfiguracionCSS.css" rel="stylesheet" type="text/css" />
	<link href="css/MonitoreoCSS.css" rel="stylesheet" type="text/css" />
	<link href="css/MiCuenta.css" rel="stylesheet" type="text/css" />
	<link href="css/loaders_spinners.css" rel="stylesheet" type="text/css" />
	<!-- FONT AWESOME -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr"
	 crossorigin="anonymous">
	<!--<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">-->

	<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
	 crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
	 crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
	 crossorigin="anonymous"></script>
	<script language="JavaScript" type="text/javascript" src="js/ajax_form.js"></script>

       

   <!-- <script src="https://www.google.com/recaptcha/api.js?render=6LfxVpMUAAAAAJu0gwYx3hU2pYFh0ageLrkJl0eb"></script>
    <script>
        grecaptcha.ready(function () {
            grecaptcha.execute('6LfxVpMUAAAAAJu0gwYx3hU2pYFh0ageLrkJl0eb', { action: 'contact' }).then(function (token) {
                var recaptchaResponse = document.getElementById('recaptchaResponse');
                recaptchaResponse.value = token;
            });
        });
    </script> -->
	<script src='https://www.google.com/recaptcha/api.js' async defer > </script>

</head>
<html>
<head>
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
	<script type="text/javascript">
		google.charts.load('current', {
			'packages': ['corechart']
		});
		google.charts.setOnLoadCallback(drawVisualization);


		function drawVisualization() {
			var data = google.visualization.arrayToDataTable([

				// 	['Energía generada y consumida', 'Potencia Consumida', 'Potencia Generada', 'Average'],
				['', '', '', ''],
				<?php foreach ($Totales_AP0 as $key => $val) {

    $Tp = explode("-", $key);
    $F = $Tp[2] . "/" . $Tp[1];
    $LINEA = $Totales_AP1[$key] / 2; //Temporal el Linea Preguntar  ?>

				["<?php echo $F; ?>", <?php echo $Totales_AP1[$key]; ?>, <?php echo $val; ?>, <?php echo $LINEA; ?>],

				<?php }?>
			]);


			var options = {

				//chartArea:{left:0,top:10,width:"100%",height:"100%"},

				//chartArea:{left:"10%",top:"10%",width:"90%",height:"80%"}, ACTUALIZA
				chartArea: {
					left: 50,
					width: '100%'
				},

				legend: 'none',

				// title : 'Monthly Coffee Production by Country',
				title: 'kWh',
				titleTextStyle: {
					color: '#a8b7c5',
					fontName: 'Roboto',
					fontSize: '12',
					bold: true
				},


				// no me deja poner el title vertical arriba       	vAxis: {title: 'kWh',
				vAxis: {
					// 		titleTextStyle: { color: '#a8b7c5', fontName: 'Roboto', fontSize: '12', bold: true },
					textStyle: {
						color: '#a8b7c5',
						fontName: 'Roboto',
						fontSize: '12',
						bold: false
					},
					baselineColor: '#dae4ea'
				},

				hAxis: {
					textStyle: {
						color: '#a8b7c5',
						fontName: 'Roboto',
						fontSize: '12',
						bold: false
					},
					baselineColor: '#dae4ea'
				},


				//	legend: { textStyle: { color: '#f5238d', fontName: 'Roboto', fontSize: '10', bold: false } },

				tooltip: {
					textStyle: {
						fontSize: 10,
						color: '#f5238d',
						fontName: 'Roboto'
					}
				},



				bar: {
					groupWidth: 16
				},

				colors: ['#f5238d', '#00ca9d', '#4a90e2'],
				seriesType: 'bars',
				series: {
					2: {
						type: 'line'
					}
				}
			};

			var chart = new google.visualization.ComboChart(document.getElementById('chart_div'));
			chart.draw(data, options);



		}
	</script>
</head>

<body>
	<div id="chart_div"></div>
</body>

</html>
<!-- script de google charts -->
<!-- https://developers.google.com/chart/interactive/docs/gallery/combochart -->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<?php
include_once __DIR__."/../../modelos/db.class.php";
// DATOS CONFIG TRAE LOS CAMPOS DE LA TABLA DE CONFIGURACION Y LOS GUARDA EN VARIABLES DE SESSION.
include_once (__DIR__.'/../Configuracion/DatosConfig.php');

//CONTROLO SI SE DESPLIEGAN LAS OPCIONES AVANZADAS. $Miia = 1 se muestran
$result = mysqli_query($conn, "SELECT Miia FROM Configuracion WHERE IDSISTEMA='" . $_SESSION['ID_SISTEMA'] . "' LIMIT 1 ");
while ($row = mysqli_fetch_array($result)) {
    $Miia = $row["Miia"];
}
$PotNom = (float)$_SESSION["Acsgfpn"];// Acsgfpn
// PRODUCTIVIDAD ENERGETICA ANUAL, ES LA MISMA EN TODOS LOS GRAFICOS.
$hoy = date('Y-m-d');
$fecha1 = date('Y-m-d', strtotime($hoy. ' -365 days'));
$query = "SELECT FECHA, SUM(EH1), SUM(EH0) FROM registro  
WHERE HW ='".$_SESSION['ID_SISTEMA']."'  AND FECHA >= '$fecha1'  AND FECHA <= '$hoy' AND AP0!=' '";
//var_dump($query);
$result = mysqli_query($conn, $query);
$Total_Generado_ano = 0;
while ($row = mysqli_fetch_array($result)) {
    $Total_Generado_ano += $row['SUM(EH0)'] ;
}
$Productividad_A = $Total_Generado_ano/$PotNom;

// PRODUCTIVIDAD ENERGETICA MENSUAL, ES LA MISMA EN TODOS LOS GRAFICOS.
$fecha1 = date('Y-m-d', strtotime($hoy. ' -30 days'));
$query = "SELECT FECHA, SUM(EH1), SUM(EH0) FROM registro 
WHERE HW ='".$_SESSION['ID_SISTEMA']."'  AND FECHA >= '$fecha1'  AND FECHA <= '$hoy' AND AP0!=' '";
//var_dump($query);
$result = mysqli_query($conn, $query);
$Total_Generado_mes = 0;
while ($row = mysqli_fetch_array($result)) {
    $Total_Generado_mes += $row['SUM(EH0)'] ;
}
$Productividad_M = $Total_Generado_mes/$PotNom;

?>

<div id="actualiza">
    <?php
    if ($_GET['M'] == "" or !isset($_GET['M']) or $_GET['M'] == "Mon_Diario") {
        $Sel_M[1] = $Style_MM;
        $Sel_MA[1] = $Style_MMA;
        $Sel_Grafica = 1;
        //confirmo si se debe graficar
        include_once(__DIR__.'/DatosMonitoreo_D.php');
        include_once(__DIR__.'/Mon_Diario.php');
    } elseif ($_GET['M'] == "Mon_Mensual") {
        $Sel_M[2] = $Style_MM;
        $Sel_MA[2] = $Style_MMA;
        $Sel_Grafica = 2;

        include_once(__DIR__.'/DatosMonitoreo_M.php');
        include_once(__DIR__.'/Mon_Diario.php');
    } elseif ($_GET['M'] == "Mon_Anual") {
        $Sel_M[3] = $Style_MM;
        $Sel_MA[3] = $Style_MMA;
        $Sel_Grafica = 3;

        include_once(__DIR__.'/DatosMonitoreo_A.php');
        include_once(__DIR__.'/Mon_Diario.php');
    } elseif ($_GET['M'] == "Mon_Historico") {
        $Sel_M[4] = $Style_MM;
        $Sel_MA[4] = $Style_MMA;
        $Sel_Grafica = 4;
        include_once(__DIR__.'/DatosMonitoreo_H.php');
        include_once(__DIR__.'/Mon_Diario.php');
    }

    ?>
</div>
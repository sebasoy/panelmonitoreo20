<?php
$fecha1 = '2018-09-01';
$fecha2 = '2018-09-30';
$PotNom = 500;

$db = new db\db\db();
$conn = $db->connectDB();

//  ULTIMO REGISTRO COMPLETO *************************************************************************************
//*********************************************************************************

$query = "SELECT * FROM registro WHERE V0!=' ' AND HW ='" . $_SESSION['ID_SISTEMA'] . "' ORDER BY ID DESC limit 1";
$resultado = mysqli_query($conn, $query);

while ($fila = mysqli_fetch_assoc($resultado)) {
    $tension = $fila['V0'];
    $corriente = $fila['I0'];
    $corriente2 = $fila['I1'];
    $AP0 = $fila['AP0'];
    $AP1 = $fila['AP1'];
    $RP0 = $fila['RP0'];
    $RP1 = $fila['RP1'];
}
// formateo los actuales
if ($RP1 < 10) {
    $RP1 = 0;
}
if ($RP0 < 10) {
    $RP0 = 0;
}

// SUMA DE LOS TOTALES  EH1 EH0***********************************************************
//***********************************************************************************
$query = "SELECT FECHA, SUM(EH1), SUM(EH0) FROM registro USE INDEX (HW_FECHA_EH1_EH0) WHERE  HW = '" . $_SESSION['ID_SISTEMA'] . "' GROUP BY FECHA";
$result = mysqli_query($conn, $query);

while ($row = mysqli_fetch_array($result)) {
    //$Totales_EH0_M[$row['FECHA']] = $row['SUM(EH0)'] / 1000; /* ********** paso de Watts a Kilo Watts  */
    //$Totales_EH1_M[$row['FECHA']] = $row['SUM(EH1)'] / 1000; /* ********** paso de Watts a Kilo Watts  */
    $Total_Consumo = $row['SUM(EH1)'] + $Total_Consumo;
    $Total_Generado = $row['SUM(EH0)'] + $Total_Generado;
}
$t = round($Total_Generado / 1000);
$t2 = round($Total_Consumo / 1000);
$porcentaje_consumo_equivalente = round($t / $t2 * 100, 2);

$Productividad_M = $Total_Generado / $PotNom;
$Total_Consumo = round($Total_Consumo / 1000); /* ********** paso de Watts a Kilo Watts  */
$Total_Generado = round($Total_Generado / 1000); /* ********** paso de Watts a Kilo Watts  */

// EQUIVALENCIAS GENERALES  *******************************************************
//*********************************************************************************
//FUNCION HORAS - DIAS - AÑOS
function Periodo($arg_1)
{
    if ($arg_1 > 720) {
        $ArgDias = round($arg_1 / 24);
        if ($ArgDias > 730) {
            $ArgAno = round($ArgDias / 365);
            return round($ArgAno) . " Años";
        } else {
            return round($ArgDias) . " Días";
        }
    } else {
        return round($arg_1) . " Hs";
    }
}

$LAMPARA = Periodo($Total_Generado / 0.012);
$HELADERA = Periodo($Total_Generado / 0.098);
$TV = Periodo($Total_Generado / 0.100);
$MICROONDAS = Periodo($Total_Generado / 0.640);
$NOTEBOOK = Periodo($Total_Generado / 0.060);
$AhorroEmisiones = round($Total_Generado * 0.532);
$AhorroAuto = round($AhorroEmisiones / 0.115);
$AhorroArboles = round($AhorroEmisiones / 10);

<?php
if (!isset($_SESSION['contador_mensual']) || (!isset($_GET['G']) && $_GET['S'] == 'graficar') || (isset($_GET['G']) && $_GET['G'] == 'reset')){
    $_SESSION['contador_mensual'] = 0;
    $contador = $_SESSION['contador_mensual'];
    $fecha1 = date("Y-m" . '-1');
    $hoy = date("Y-m-d");
}else{
    $contador = $_SESSION['contador_mensual'];
    if(isset($_GET['G']) && $_GET['G'] == 'next' && $contador < 0 ){
        $contador = $contador + 1;
    }
    if(isset($_GET['G']) && $_GET['G'] == 'prev' ){
        $contador = $contador - 1;

    }
    //var_dump($contador);
    $fecha1 = date("Y-m-01");
    $fecha1 = date('Y-m-d', strtotime($fecha1. ' '.$contador.' months'));
    $hoy = date('Y-m-t', strtotime($fecha1));
    $_SESSION['contador_mensual'] = $contador;
}

//$PotNom = 500;

// Mensual / Anual / Historico (auto consumo es por hora y no por dia - sumar por hora )
$query = "SELECT FECHA, SUM(EH1), SUM(EH0)  FROM registro WHERE HW ='" . $_SESSION['ID_SISTEMA'] . "'  AND FECHA >= '$fecha1'  
AND FECHA <= '$hoy'  GROUP BY FECHA order by ID asc ";
$result = mysqli_query($conn, $query);
//var_dump($query);
$Total_Consumo = 0;
$Total_Generado = 0;
$autoconsumo = 0;
while ($row = mysqli_fetch_array($result)) {
    $Totales_EH0_M[$row['FECHA']] = $row['SUM(EH0)'] / 1000; /* ********** paso de Watts a Kilo Watts  */
    $Totales_EH1_M[$row['FECHA']] = $row['SUM(EH1)'] / 1000; /* ********** paso de Watts a Kilo Watts  */
    $Total_Consumo = $row['SUM(EH1)'] + $Total_Consumo;
    $Total_Generado = $row['SUM(EH0)'] + $Total_Generado;
}
//$Productividad_M = $Total_Generado / $PotNom;

//////////////////// Si info avanzada esta activo hago la busqueda  ///////////////////////////////////////
if ($Miia == "1") {
    $query2 = "select FECHA, HORA, RP1, I0, I1, RP0, RP1, V0 from registro 
                 WHERE HW ='".$_SESSION['ID_SISTEMA']."'  AND FECHA >= '$fecha1'  AND FECHA <= '$hoy' and AP0 != ' ' 
                 order by Fecha, HORA desc";
    //var_dump($query2);
    $resultado = mysqli_query($conn, $query2);
    $Max_Generacion_C = 0;
    $Max_Generacion_C_fecha_hora = '';
    $Max_Generacion_Co = 0;
    $Max_Consumo_C = 0;
    $Max_Consumo_C_fecha_hora = '';
    $Max_Consumo_Co = 0;
    $Max_Tension = 0;
    $Min_Tension = 300;
    $flag = 0;
    while ($fila = mysqli_fetch_array($resultado)) {


        if ($Max_Generacion_C < $fila['RP0']) {$Max_Generacion_C = $fila['RP0']; $Max_Generacion_C_fecha_hora = substr($fila['FECHA'],5,5)." / ".substr($fila['HORA'],0,5);};
        if ($Max_Generacion_Co < $fila['I0']) $Max_Generacion_Co = $fila['I0'];
        if ($Max_Consumo_C < $fila['RP1']) {$Max_Consumo_C = $fila['RP1']; $Max_Consumo_C_fecha_hora= substr($fila['FECHA'],5,5)." / ".substr($fila['HORA'],0,5);};
        if ($Max_Consumo_Co < $fila['I1']) $Max_Consumo_Co = $fila['I1'];
        if ($Max_Tension < $fila['V0']) $Max_Tension = $fila['V0'];
        if ($Min_Tension > $fila['V0'] && $fila['V0'] != '') $Min_Tension = $fila['V0'];
        if ($fila['RP0'] != 0 && $flag == 0){
            $RP0  = $fila['RP0'];
            $RP1  = $fila['RP1'];
            $flag = 1;
        }
    }

    //CALCULO AUTOCONSUMO CALCULO AUTOCONSUMO CALCULO AUTOCONSUMO CALCULO AUTOCONSUMO CALCULO AUTOCONSUMO CALCULO AUTOCONSUMO
    $query4 = "SELECT HORA, AP1, AP0 FROM registro USE INDEX (HW_FECHA_AP0) 
               WHERE HW ='" . $_SESSION['ID_SISTEMA'] . "'  AND AP0!=' ' AND
               FECHA >= '$fecha1'  AND FECHA <= '$hoy' 
               order by ID asc";
    $result = mysqli_query($conn, $query4);
    $autoconsumo = 0;
    while ($row = mysqli_fetch_array($result)) {
        if ($row['AP0'] <=  $row['AP1']) $autoconsumo += $row['AP0']/12;
        else $autoconsumo += $row['AP1']/12;
    }

} ?>
<?php ///////////////////////////////////////////////////////////////////////////////////////////////////////////////?>
<script type = "text/javascript" >
    google.charts.load('current', {
        'packages': ['corechart']
    });
    google.charts.setOnLoadCallback(drawVisualization);


    function drawVisualization() {
        var data = google.visualization.arrayToDataTable([
            //['Energía generada y consumida', 'Potencia Consumida', 'Potencia Generada', 'Average'],
            ['', '', '', ''],
            <?php foreach ($Totales_EH0_M as $key => $val) {
            $Tp = explode("-", $key);
            $F = $Tp[2];
            //$F = $Tp[2] . "/" . $Tp[1];

            if ($Totales_EH0_M[$key] < $Totales_EH1_M[$key]) {
                $LINEA = $Totales_EH0_M[$key];
            } else {
                $LINEA = $Totales_EH1_M[$key];
            } ?>

            ["<?= $F; ?>", <?= (int)$Totales_EH1_M[$key]; ?>, <?= (int)$val; ?>, <?= (int)$LINEA; ?>],

            <?php
            }?>
        ]);

        var options = {
            //chartArea:{left:0,top:10,width:"100%",height:"100%"},
            //chartArea:{left:"10%",top:"10%",width:"90%",height:"80%"},
            height: 400,
            chartArea: {
                left: 50, /*te mueve el grafico 50px a la izquierda*/
                right: 50, /*te mueve el grafico 50px a la derecha*/
                width: '100%',
                //height: '90%',
            },

            legend: 'none',

            // title : 'Monthly Coffee Production by Country',
            title: 'kWh',
            titleTextStyle: {
                color: '#a8b7c5',
                fontName: 'Roboto',
                fontSize: '12',
                bold: true
            },


            // no me deja poner el title vertical arriba       	vAxis: {title: 'kWh',
            vAxis: {
                // 		titleTextStyle: { color: '#a8b7c5', fontName: 'Roboto', fontSize: '12', bold: true },
                textStyle: {
                    color: '#a8b7c5',
                    fontName: 'Roboto',
                    fontSize: '12',
                    bold: false
                },
                baselineColor: '#dae4ea'
            },

            hAxis: {
                textStyle: {
                    color: '#a8b7c5',
                    fontName: 'Roboto',
                    fontSize: '12',
                    bold: false
                },
                baselineColor: '#dae4ea'
            },
            //	legend: { textStyle: { color: '#f5238d', fontName: 'Roboto', fontSize: '10', bold: false } },
            tooltip: {
                textStyle: {
                    fontSize: 14,
                    color: '#f5238d',
                    fontName: 'Roboto'
                }
            },
            bar: {
                groupWidth: 36
            },
            colors: ['#f5238d', '#00ca9d', '#4a90e2'],
            seriesType: 'bars',
            series: {
                2: {
                    type: 'line'
                }
            }
        };

        var chart = new google.visualization.ComboChart(document.getElementById('chart_div'));
        google.visualization.events.addListener(chart, 'ready', selectHandler);
        chart.draw(data, options);
        function selectHandler(e) {
            <?php if($auto_scroll_al_grafico) { ?>
            $("html, body").animate({ scrollTop: $(document).height() }, 2000);
            <?php } ?>
        }
    }


</script>
<div id="esconder_graficos" <?= $hide_grafico ?>>
    <div class="wrap_flechas_grafico" >
        <a href="index.php?P=Monitoreo&M=Mon_Mensual&S=graficar&G=prev"><i class="fas fa-angle-left"></i></a>
        <span style="font-size: 14px;opacity: 0.7;">
            <?php
            //setlocale(LC_ALL , '');
            setlocale(LC_ALL,"es_ES");
            $now = time() + 86400 * $contador * 30;
            echo strtoupper(utf8_encode((strftime("%b  %Y",$now))));
            ?>
        </span>
        <?php if (!$_SESSION['contador_mensual'] == 0){ ?>
            <a href="index.php?P=Monitoreo&M=Mon_Mensual&S=graficar&G=next"> <i class="fas fa-angle-right"></i></a>
            <a href="index.php?P=Monitoreo&M=Mon_Mensual&S=graficar&G=reset" title="Volver al mes actual"><i class="fas fa-angle-right"></i><i class="fas fa-angle-right"></i> </a>
        <?php } ?>
    </div>
    <div style="margin-top: 50px;">
        <div id="chart_div" style="min-width: 100%; max-width:100%; height: 400px">
            <div class="loader_azul_grande"></div>
        </div>
        <div class="nombre_eje_x">Dia</div>
    </div>
    <div id="wrap_datos_debajo_grafico">
        <div class="Contedesde">Desde dı́a 1 hasta dı́a actual.</div>
        <div class="ContOpc100">
            <div class="ContOpcMuchas_MD">
                <div class="Cont_D"><img src="Img/consumo-icon.png" /></div>
                <div class="Cont_I">
                    <p id="Cont_I_T">Consumo </p>
                    <p id="Cont_I_Te">Total:
                        <?php if($Total_Consumo > 1000) {
                            $Total_Consumo = $Total_Consumo/1000;
                            echo round($Total_Consumo,1)." KWh <br/>";
                        }else{
                            echo round($Total_Consumo,1)." Wh <br/>";
                        }?>
                        Actual: <?= round($RP1); ?> W <br/>
                        <?php if ($Miia=="1") { ?>
                            Máx.: <?= round($Max_Consumo_C, 1); ?> W <br/>
                            <b> <?= $Max_Consumo_C_fecha_hora ?> </b>
                            <?php
                        } ?>
                    </p>
                </div>
            </div>
            <div class="ContOpcMuchas_MD">
                <div class="Cont_D"><img src="Img/Generacion-icon.png" /></div>
                <div class="Cont_I">
                    <p id="Cont_I_T">Generación </p>
                    <p id="Cont_I_Te">Total:
                        <?php if($Total_Generado > 1000) {
                            $Total_Generado = $Total_Generado/1000;
                            echo round($Total_Generado,1)." KWh <br/>";
                        }else{
                            echo round($Total_Generado,1)." Wh <br/>";
                        }?>
                        <?= "Actual:".round($RP0,1); ?> W <br/>
                        <?php if ($Miia=="1") { ?>
                            Máx.: <?= round($Max_Generacion_C, 1); ?> W <br/>
                            <b><?= $Max_Generacion_C_fecha_hora ?></b>
                            <?php
                        } ?>
                    </p>
                </div>
            </div>
            <?php ////////////////  SI NO ESTA HABILITADA LA INFORMACION AVANZADA NO SE MUESTRA
            if ($Miia == "1") {

                ?>
                <div class="ContOpcMuchas_MD">
                    <div class="Cont_D"><img src="Img/Inyeccion-icon.png" /></div>
                    <div class="Cont_I">
                        <p id="Cont_I_T">Autoconsumo </p>
                        <p id="Cont_I_Te">
                            <?php if($autoconsumo > 1000) {
                                $autoconsumo = $autoconsumo/1000;
                                echo "<b>".round($autoconsumo,1)."</b> KWh";
                            }else{
                                echo "<b>".round($autoconsumo,1)."</b> Wh";
                            }?>
                        <i class="fas fa-question-circle" 
                        title="Refiere a la energía que generaste y consumiste el mismo momento " 
                        style="font-weight: 900;font-size: 16px;color: #0904e8ad;"></i></p>
                    </div>
                </div>
                <div class="ContOpcMuchas_MD">
                    <div class="Cont_D"><img src="Img/tension-icon.png" /></div>
                    <div class="Cont_I">
                        <p id="Cont_I_T">Tensión </p>
                        <p id="Cont_I_Te">Máxima:
                            <?= round($Max_Tension, 1); ?> V <br/>Mínima:
                            <?= round($Min_Tension, 1); ?> V <br/>Actual:
                            <?= round($tension, 1); ?> V</p>
                    </div>
                </div>
                <div class="ContOpcMuchas_MD">
                    <div class="Cont_D"><img src="Img/Corriente-icon.png" /></div>
                    <div class="Cont_I">
                        <p id="Cont_I_T">Máx de corriente </p>
                        <p id="Cont_I_Te">Generación:
                            <?= round($Max_Generacion_Co, 1); ?> A <br/>Consumo:
                            <?= round($Max_Consumo_Co, 1); ?> A</p>
                    </div>
                </div>
                <div class="ContOpcMuchas_MD">
                    <div class="Cont_D"><img src="Img/productividad-icon.png" /></div>
                    <div class="Cont_I">
                        <p id="Cont_I_T">Productividad </p>
                        <p id="Cont_I_Te">
                            <b><?= round($Productividad_A,1); ?></b> kWh/kWp año <br/>
                            <b><?php echo round($Total_Generado_mes/$PotNom,1) ?></b> kWh/kWp mes
                        </p>
                    </div>
                </div>
                <?php
            } ////////////// fin de la info avanzada ////////////?>
        </div>
    </div>
</div>
</div>


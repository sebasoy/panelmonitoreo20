<?php
////////////////////////////////////////////////// conecta y consulta //////////////////////////////////////////
if (!isset($_SESSION['contador_diario']) || (!isset($_GET['G']) && ($_GET['S'] == 'graficar' )) || (isset($_GET['G']) && $_GET['G'] == 'reset')){
    $_SESSION['contador_diario'] = 0;
    $contador = $_SESSION['contador_diario'];
    $hoy = date("Y-m-d");
}else{
    $contador = $_SESSION['contador_diario'];
    if(isset($_GET['G']) && $_GET['G'] == 'next' && $contador < 0 ){
        $contador = $contador + 1;
    }
    if(isset($_GET['G']) && $_GET['G'] == 'prev' ){
        $contador = $contador - 1;
    }
    //var_dump($contador);
    $hoy = date("Y-m-d");
    $hoy = date('Y-m-d', strtotime($hoy. ' '.$contador.' days'));
    $_SESSION['contador_diario'] = $contador;
}

$PotNom = 500;
$query = "SELECT FECHA, SUM(EH1), SUM(EH0) FROM registro WHERE HW ='" . $_SESSION['ID_SISTEMA'] . "'  AND AP0!=' ' AND FECHA = '$hoy'  order by ID asc";
$result = mysqli_query($conn, $query);
//var_dump($query);
$Total_Consumo = 0;
$Total_Generado = 0;
while ($row = mysqli_fetch_array($result)) {
    $Total_Consumo  = $row['SUM(EH1)'];
    $Total_Generado = $row['SUM(EH0)'];
}

$query = "SELECT HORA, AP1, AP0 FROM registro USE INDEX (HW_FECHA_AP0) WHERE HW ='" . $_SESSION['ID_SISTEMA'] . "'  AND AP0!=' ' AND FECHA = '$hoy' order by ID asc";
/* esta query saca el promedio por hora
$query = "SELECT HORA, avg(AP1) AP1, avg(AP0) AP0 FROM registro USE INDEX (HW_FECHA_AP0)
WHERE HW ='" . $_SESSION['ID_SISTEMA'] . "'  AND AP0!=' ' AND FECHA = '$hoy'  group by hour( HORA ) order by ID asc";*/
$result = mysqli_query($conn, $query);
//var_dump($query);
//var_dump($result);
$autoconsumo = 0;
while ($row = mysqli_fetch_array($result)) {
    if ($row['AP0'] <=  $row['AP1']){ $autoconsumo += $row['AP0']/12; /*var_dump($row['AP0']);*/ }
    else{ $autoconsumo += $row['AP1']/12; /*var_dump($row['AP0']);*/ }
    $Totales_AEH0[$row['HORA']] = $row['AP0'];
    $Totales_AEH1[$row['HORA']] = $row['AP1'];
}

//////////////////// Si info avanzada esta activo hago la busqueda  ///////////////////////////////////////
if ($Miia == "1") {
    $query2 = "select FECHA, HORA, RP1, I0, I1, RP0, RP1, V0 from registro 
                     WHERE HW ='".$_SESSION['ID_SISTEMA']."'  AND FECHA = '$hoy' and AP0 != ' '
                     order by Fecha, HORA desc";
    //var_dump($query2);
    $resultado = mysqli_query($conn, $query2);
    //var_dump(mysqli_fetch_array($resultado));
    $Max_Generacion_C = 0;
    $Max_Generacion_C_fecha_hora = '';
    $Max_Generacion_Co = 0;
    $Max_Consumo_C = 0;
    $Max_Consumo_C_fecha_hora = '';
    $Max_Consumo_Co = 0;
    $Max_Tension = 0;
    $Min_Tension = 300;
    $flag = 0;
    while ($fila = mysqli_fetch_array($resultado)) {
        //var_dump($fila);
        if ($Max_Generacion_C < $fila['RP0']) {$Max_Generacion_C = $fila['RP0']; $Max_Generacion_C_fecha_hora = substr($fila['HORA'],0,5);};
        if ($Max_Generacion_Co < $fila['I0']) $Max_Generacion_Co = $fila['I0'];
        if ($Max_Consumo_C < $fila['RP1']) {$Max_Consumo_C = $fila['RP1']; $Max_Consumo_C_fecha_hora= substr($fila['HORA'],0,5);};
        if ($Max_Consumo_Co < $fila['I1']) $Max_Consumo_Co = $fila['I1'];
        if ($Max_Tension < $fila['V0']) $Max_Tension = $fila['V0'];
        if ($Min_Tension > $fila['V0'] && $fila['V0'] != '') $Min_Tension = $fila['V0'];
        if ($fila['RP0'] != 0 && $flag == 0){
            $RP0  = $fila['RP0'];
            $RP1  = $fila['RP1'];
            $flag = 1;
        }
    }

}

?>

<script type="text/javascript">
    google.charts.load('current', {
        'packages': ['corechart']
    });
    google.charts.setOnLoadCallback(drawVisualization);

    function drawVisualization() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'x');
        data.addColumn('number', 'values');
        data.addColumn({
            id: 'i2',
            type: 'number',
            role: 'interval'
        });
        data.addColumn({
            id: 'i2',
            type: 'number',
            role: 'interval'
        });
        data.addColumn('number', 'values');
        data.addColumn({
            id: 'i3',
            type: 'number',
            role: 'interval'
        });
        data.addColumn({
            id: 'i3',
            type: 'number',
            role: 'interval'
        });

        data.addRows([
            <?php foreach ($Totales_AEH0 as $key => $val) { ?>
            [
                <?= "'".substr($key, 0, -3)."'" ; ?> , // hora
                <?= $Totales_AEH1[$key]; ?> ,//consumida
                <?= $Totales_AEH1[$key]; ?> ,//consumida
                <?= $val; ?> ,//generada
                <?= $val; ?> ,//generada
                <?= $val; ?> ,//generada
                0
            ],
            <?php } ?>
        ]);
        //MOSTRAR EN LA CONSOLA LOS VALORES.
        var datos = [];
        <?php foreach ($Totales_AEH0 as $key => $val) { ?>
        var linea = [
            <?="'" . substr($key, 0, -3) . "'"; ?> ,
            <?= $Totales_AEH1[$key]; ?> ,
            <?= $Totales_AEH1[$key]; ?> ,
            <?= $val; ?> ,
            <?= $val; ?> ,
            <?= $val; ?> ,
            0
        ];
        datos.push(linea);
        <?php
        }?>
        console.log(datos);

        var options = {
            //curveType: 'function',
            chartArea: {
                left: 50, /*te mueve el grafico 50px a la izquierda*/
                right: 50, /*te mueve el grafico 50px a la derecha*/
                width: '100%',
                /*height: '90%',
                curveType: 'function',*/
            },
            //theme: 'maximized',

            legend: 'none',

            title: 'W',
            titleTextStyle: {
                color: '#a8b7c5',
                fontName: 'Roboto',
                fontSize: '12',
                bold: true
            },

            vAxis: {
                textStyle: {
                    color: '#a8b7c5',
                    fontName: 'Roboto',
                    fontSize: '12',
                    bold: false
                },
                viewWindowMode: 'explicit',
                viewWindow: {
                    min: 0,
                },
                baselineColor: '#dae4ea'
            },

            hAxis: {
                viewWindowMode: 'maximized',
                textStyle: {
                    color: '#a8b7c5',
                    fontName: 'Roboto',
                    fontSize: '12',
                    bold: false
                },
                baselineColor: '#dae4ea'
            },

            tooltip: {
                textStyle: {
                    fontSize: 14,
                    color: '#f5238d',
                    fontName: 'Roboto'
                }
            },

            colors: ['#f5238d', '#00ca9d'],
            intervals: {
                'color': 'series-color'
            },
            interval: {
                'i2': {
                    'color': '#fee9f3',
                    'style': 'area',
                    'curveType': 'function',
                    'fillOpacity': 0.9
                },
                'i3': {
                    'color': '#cbe2e1',
                    'style': 'area',
                    'curveType': 'function',
                    'fillOpacity': 0.9
                }
            },

        };

        var chart_lines = new google.visualization.LineChart(document.getElementById('chart_div'));
        google.visualization.events.addListener(chart_lines, 'ready', selectHandler);
        chart_lines.draw(data, options);
        function selectHandler(e) {
            <?php if($auto_scroll_al_grafico) { ?>
            $("html, body").animate({ scrollTop: $(document).height() }, 2000);
            <?php } ?>
        }
    }
</script>

<div id="esconder_graficos" <?= $hide_grafico ?>>
    <div  class="wrap_flechas_grafico" >
        <a href="index.php?P=Monitoreo&M=Mon_Diario&S=graficar&G=prev"><i class="fas fa-angle-left"></i></a>
        <span style="">
                <?php
                setlocale(LC_ALL,"es_ES");
                $now = time() + 86400 * $contador;
                echo ucwords(utf8_encode((strftime("%a %d %b  %Y",$now))));
                ?>
            </span>
        <?php if (!$_SESSION['contador_diario'] == 0){ ?>
            <a href="index.php?P=Monitoreo&M=Mon_Diario&S=graficar&G=next"> <i class="fas fa-angle-right"></i></a>
            <a href="index.php?P=Monitoreo&M=Mon_Diario&S=graficar&G=reset" title="Volver a la fecha actual"><i class="fas fa-angle-right"></i><i class="fas fa-angle-right"></i> </a>
        <?php } ?>
    </div>
    <div style="margin-top: 50px;">
        <div id="chart_div" style="min-width: 100%; max-width:100%; height: 400px">
            <div class="loader_azul_muy_grande"></div>
        </div>
        <div class="nombre_eje_x">Hs.</div>
    </div>
    <div id="wrap_datos_debajo_grafico" >
        <div class="Contedesde">Desde las 00.00 a horario actual.</div>

        <div class="ContOpc100">
            <div class="ContOpcMuchas_MD">
                <div class="Cont_D"><img src="Img/consumo-icon.png" /></div>
                <div class="Cont_I">
                    <p id="Cont_I_T">Consumo </p>
                    <p id="Cont_I_Te">Total: <?php  ?>
                        <?php if($Total_Consumo > 1000) {
                            $Total_Consumo = $Total_Consumo/1000;
                            echo round($Total_Consumo,1)." KWh <br/>";
                        }else{
                            echo round($Total_Consumo,1)." Wh <br/>";
                        }?>
                        <?= "Actual:".round($RP1); ?> W <br/>
                        <?php if ($Miia=="1") { ?>
                            Máx.: <?= round($Max_Consumo_C, 1); ?> W <br/>
                            <b> <?= $Max_Consumo_C_fecha_hora ?> </b>
                            <?php
                        } ?>
                    </p>
                </div>
            </div>
            <div class="ContOpcMuchas_MD">
                <div class="Cont_D"><img src="Img/Generacion-icon.png" /></div>
                <div class="Cont_I">
                    <p id="Cont_I_T">Generación </p>
                    <p id="Cont_I_Te">Total:
                        <?php if($Total_Generado > 1000) {
                            $Total_Generado = $Total_Generado/1000;
                            echo round($Total_Generado,1)." KWh <br/>";
                        }else{
                            echo round($Total_Generado,1)." Wh <br/>";
                        }?>
                        <?= "Actual:".round($RP0); ?> W <br/>
                        <?php if ($Miia=="1") { ?>
                            Máx.: <?= round($Max_Generacion_C, 1); ?> W <br/>
                            <b><?= $Max_Generacion_C_fecha_hora ?></b>
                            <?php
                        } ?>
                    </p>
                </div>
            </div>
            <?php ////////////////  SI NO ESTA HABILITADA LA INFORMACION AVANZADA NO SE MUESTRA
            if ($Miia == "1") {
                ?>
                <div class="ContOpcMuchas_MD">
                    <div class="Cont_D"><img src="Img/Inyeccion-icon.png" /></div>
                    <div class="Cont_I">
                        <p id="Cont_I_T">Autoconsumo </p>
                        <p id="Cont_I_Te">
                            <?php if($autoconsumo > 1000) {
                                $autoconsumo = $autoconsumo/1000;
                                echo "<b>".round($autoconsumo,1)."</b> KWh";
                            }else{
                                echo "<b>".round($autoconsumo,1)."</b> Wh";
                            }?>
                        <i class="fas fa-question-circle" 
                        title="Refiere a la energía que generaste y consumiste el mismo momento " 
                        style="font-weight: 900;font-size: 16px;color: #0904e8ad;"></i></p>
                    </div>
                </div>
                <div class="ContOpcMuchas_MD">
                    <div class="Cont_D"><img src="Img/tension-icon.png" /></div>
                    <div class="Cont_I">
                        <p id="Cont_I_T">Tensión </p>
                        <p id="Cont_I_Te">Máxima:
                            <?= round($Max_Tension, 1); ?> V <br/>Mínima:
                            <?= round($Min_Tension, 1); ?> V <br/>Actual:
                            <?= round($tension, 1); ?> V</p>
                    </div>
                </div>
                <div class="ContOpcMuchas_MD">
                    <div class="Cont_D"><img src="Img/Corriente-icon.png" /></div>
                    <div class="Cont_I">
                        <p id="Cont_I_T">Máx de corriente </p>
                        <p id="Cont_I_Te">Generación:
                            <?= round($Max_Generacion_Co, 1); ?> A <br/>Consumo:
                            <?= round($Max_Consumo_Co, 1); ?> A</p>
                    </div>
                </div>
                <div class="ContOpcMuchas_MD">
                    <div class="Cont_D"><img src="Img/productividad-icon.png" /></div>
                    <div class="Cont_I">
                        <p id="Cont_I_T">Productividad </p>
                        <p id="Cont_I_Te">
                            <b><?= round($Productividad_A,1); ?></b> kWh/kWp año <br/>
                            <b><?php echo round($Total_Generado_mes/$PotNom,1) ?></b> kWh/kWp mes
                        </p>
                    </div>
                </div>
                <?php
            } ////////////// fin de la info avanzada ////////////?>
        </div>
    </div>
</div>
</div>
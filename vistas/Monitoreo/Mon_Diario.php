<div class="ContenidoOpcion_M" style="">
    <div class="ConteResumen_I_M">Resumen hist&oacute;rico <i class="fas fa-question-circle" 
        title="Valores totales desde la instalación de cbox" 
        style="font-weight: 900;font-size: 16px;color: #0904e8ad;"></i>
    </div>
    <div class="ContOpc3M">
        <div id="ConteOpText_RH">
            <img src="Img/solar-panel-icon.png" />
            <?php echo round($porcentaje_consumo_equivalente)."%"; ?>
            <?php if($porcentaje_consumo_equivalente > 100){ $porcentaje_consumo_equivalente = 102; }?>
            <p>de tu consumo fue cubierto con tu sistema FV.</p>
            <div id="Barra_RH">
                <div id="line-2" ></div>
                <div id="line-2X" style="width: <?= round($porcentaje_consumo_equivalente)?>%"> </div>
            </div>

            <img src="Img/ahorro-icon.png" width="48" height="48" />
            <div style="display: inline-block;color:#4A4A4A;"> $<?= $Total_Generado * $_SESSION["Cpe"] ?></div>
            <p>Ahorro económico estimado <i class="fas fa-question-circle" title="Total histórico estimado según valor actual de energía ($/hWh) " style="font-weight: 900;font-size: 16px;color: #0904e8ad;"></i></p>

        </div>
    </div>
    <!-- ENERGIA GENERADA ENERGIA GENERADA ENERGIA GENERADA ENERGIA GENERADA ENERGIA GENERADA ENERGIA GENERADA ENERGIA GENERADA ENERGIA GENERADA -->
    <div class="ContOpc3M">
        <div class="rectangle-V">
            <div class="el-sistema-esta-func">
                <img src="Gif/generacion.gif" width="200" height="55" />
                <p class="energia-generada">Energía Generada</p>
                <p class="kwhV">
				<span style="display: inline-block;font-size:40px;">
					<?php echo number_format(round($Total_Generado), 0, '.', '.'); ?>
				</span> kWh
                </p>
                <!--<i class="far fa-smile" style="font-size: 24px;color: #00CA9D;margin-bottom: 10px;"></i><br>
                El sistema está<br>funcionando correctamente-->
            </div>
            <div style="    margin-top: 224px;width: 245px;position: absolute;">
                <div class="FondoNoGradiente_background" style="">&nbsp;</div>
                <div class="FondoNoGradiente" style="width: <?= round($porcentaje_consumo_equivalente)?>%">&nbsp;</div>
            </div>
        </div>
    </div>
    <!-- ENERGIA CONSUMIDA ENERGIA CONSUMIDA ENERGIA CONSUMIDA ENERGIA CONSUMIDA ENERGIA CONSUMIDA ENERGIA CONSUMIDA ENERGIA CONSUMIDA ENERGIA CONSUMIDA -->
    <div class="ContOpc3M">
        <div class="rectangle-R">
            <div class="el-sistema-esta-func">
                <img src="Gif/consumo.gif" width="200" height="55" />
                <p class="energia-generada">Energía Consumida</p>
                <p class="kwhR">
				<span style="display: inline-block;font-size:40px;">
					<?php echo number_format(round($Total_Consumo), 0, '.', '.'); ?>
				</span> kWh
                </p>
                <!--<i class="far fa-meh" style="font-size: 24px;color: #F5238D;margin-bottom: 10px;"></i>
               <i class="far fa-frown" style="font-size: 24px;color: #F5238D;margin-bottom: 10px;"></i><br>
               <b>Cuidado</b><br />Tu consumo hasta hoy es mayor</br>al medio promedio-->
            </div>
            <div class="FondoGradiente">&nbsp;</div>
        </div>
    </div>

    <div class="ContOpc100" style="margin-top:20px;border-bottom: 1px solid #A8B7C5;">
        <div class="ahorros-y-equivalencM">Ahorros y equivalencias</div>
        <!--<div class="historicoM">Histórico</div> -->
    </div>
    <!-- AHORROS AUTO ARBOLES Y CO2 AHORROS AUTO ARBOLES Y CO2 AHORROS AUTO ARBOLES Y CO2 AHORROS AUTO ARBOLES Y CO2 AHORROS AUTO ARBOLES Y CO2 -->
    <div class="ContOpc100">
        <div class="ContOpc2_D">
            <div id="ahorro-de-emisiones">Ahorro de emisiones GEI <i class="fas fa-question-circle" 
            title="Los Gases de Efecto Invernadero son los principales responsables del Cambio Climático " 
            style="font-weight: 900;font-size: 16px;color: #0904e8ad;"></i>
            </div>
            <div id="F2984"><img src="Img/emisiones-icon.png" />
                <?php echo $AhorroEmisiones; ?>
                <font style="font-size:10pt; letter-spacing: normal;">Kg CO2 eq</font>
            </div>
            <div class="ContOpcMuchas_AH">
                <img src="Img/arboles-icon.png" />
                <p id="F48hsV">
                    <?php echo $AhorroArboles; ?>
                </p>
                <p id="F48hsTex">Arboles Plantados</p>
            </div>
            <div class="ContOpcMuchas_AH">
                <img src="Img/auto-icon.png" />
                <p id="F48hsV">
                    <?php echo $AhorroAuto; ?> km</p>
                <p id="F48hsTex">Ahorrados</p>
            </div>
            <div class="gases">* Gases de efecto invernadero y equivalentes</div>
        </div>
        <!-- LAMPARAS HELADERAS TV MICROONDAS NOTEBOOK LAMPARAS HELADERAS TV MICROONDAS NOTEBOOK LAMPARAS HELADERAS TV MICROONDAS NOTEBOOK -->
        <div class="ContOpc2_I">
            <div id="ahorro-de-emisiones">Con la energía generada podrías usar</div>

            <div class="ContOpcMuchas_M">
                <img src="Img/lampara-icon.png" />
                <p id="F48hsV">
                    <?php echo $LAMPARA; ?>
                </p>
                <p id="F48hsTex"> Lámpara </p>
            </div>
            <div class="ContOpcMuchas_M">
                <img src="Img/heladera-icon.png" />
                <p id="F48hsV">
                    <?php echo $HELADERA; ?>
                </p>
                <p id="F48hsTex"> Heladera </p>
            </div>
            <div class="ContOpcMuchas_M">
                <img src="Img/televisor-icon.png" />
                <p id="F48hsV">
                    <?php echo $TV; ?>
                </p>
                <p id="F48hsTex"> Smart TV </p>
            </div>
            <div class="ContOpcMuchas_M">
                <img src="Img/microondas-icon.png" />
                <p id="F48hsV">
                    <?php echo $MICROONDAS; ?>
                </p>
                <p id="F48hsTex"> Microondas </p>
            </div>
            <div class="ContOpcMuchas_M">
                <img src="Img/notebook-icon.png" />
                <p id="F48hsV">
                    <?php echo $NOTEBOOK; ?>
                </p>
                <p id="F48hsTex"> Notebook </p>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>

<div id="marcos">
    <div id="contenedor">
        <div class="ContOpc100">
            <div class="ConteResumen_I_M">
                <?php if ($Sel_Grafica == 1) {
                    echo "Potencia generada y consumida";
                } else {
                    echo "Energía generada y consumida";
                    //
                }
                $aut_scroll_al_grafico = false;
                if ($_GET['S'] == 'graficar') {
                    $_SESSION['Midh'] = 1;
                    $auto_scroll_al_grafico = true;
                }

                ?>
                <div class="ConteResumen_D_M">
                    <div style="float: right;">
                        <div class="rectangle-MENU">
                            <div class="rectangle-MENU-LINK" <?php echo $Sel_M[1]; ?> >
                                <a href="?P=Monitoreo&M=Mon_Diario&S=graficar" <?php echo $Sel_MA[1]; ?>
                                >Diaria</a>
                            </div>
                            <div class="rectangle-MENU-LINK" <?php echo $Sel_M[2]; ?> >
                                <a href="?P=Monitoreo&M=Mon_Mensual&S=graficar" <?php echo $Sel_MA[2]; ?>
                                >Mensual</a>
                            </div>
                            <div class="rectangle-MENU-LINK" <?php echo $Sel_M[3]; ?> >
                                <a href="?P=Monitoreo&M=Mon_Anual&S=graficar" <?php echo $Sel_MA[3]; ?>
                                >Anual</a>
                            </div>
                            <div class="rectangle-MENU-LINK" <?php echo $Sel_M[4]; ?> >
                                <a href="?P=Monitoreo&M=Mon_Historico&S=graficar" <?php echo $Sel_MA[4]; ?>
                                >Histórica</a>
                            </div>
                        </div>
                        <div class="IconoMas-MENU">
                            <div id="expandir_grafico_monitoreo">
                                <i class="fas fa-minus-circle" style="font-size: 25px;<?= $_SESSION['Midh'] ? '': 'display:none'; ?>"></i>
                                <i class="fas fa-plus-circle" style="font-size: 25px;<?= $_SESSION['Midh'] ? 'display:none': ''; ?>"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<?php

//ESCONDE EL GRAFICO DEPENDIENDO DE SI TIENE CONFIGURADA LA VISTA DEL MISMO
$hide_grafico = !$_SESSION['Midh'] ? ' style="display: none !important"; ': "" ;
//var_dump($hide_grafico);
if ($Sel_Grafica == 1) {
    include_once(__DIR__.'/GC_Diario3.php');
} elseif ($Sel_Grafica == 2) {
    include_once(__DIR__.'/GC_Mensual.php');
} elseif ($Sel_Grafica == 3) {
    include_once(__DIR__.'/GC_Anual.php');
} elseif ($Sel_Grafica == 4) {
    include_once(__DIR__.'/GC_Historico.php');
}

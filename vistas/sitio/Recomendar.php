<div class="wrap_cbox_by_pixsun">
    <?php if (isset($_GET['msj']) && $_GET['msj'] == "mail_enviado") {
        echo "<div class='wrap_msj'>
            <span class='texto_msj'>Un mail fue enviado a <b>info@pixsun.com.ar</b> y al usuario recomendado.</span>
            <i class='x_cerrar fas fa-times'></i>
        </div>";
    } ?>
    <h1>Recomendar</h1>
    <div class="cuerpo_cbox_by_pixsun">
        <p> Si crees que algún amigo, familiar o conocido está interesado en nuestros servicios; ingresa sus datos
            y
            nos
            pondremos en contacto con él. Ambos recibirán beneficios.
        </p>
        <form action="vistas/sitio/Recomendar_success.php" style="max-width: 945px">
            <div class="" style="display:flex;flex-direction: column">
                <div class="usuario_item">
                    <label for="nombre">Nombre</label>
                    <div style="width: auto">
                        <input type="text" name="nombre" class="input_U" value="" placeholder="Nombre y apellido" required>
                        <i class="input_icon fas fa-user-alt"></i>
                    </div>
                </div>
                <div class="usuario_item">
                    <label for="email">Email</label>
                    <div style="width: auto">
                        <input type="email" name="email" class="input_U" placeholder="Email" required>
                        <i class="input_icon fas fa-envelope"></i>
                    </div>
                </div>
                <div class="usuario_item">
                    <label for="telefono">Telefono</label>
                    <div style="width: auto">
                        <input type="text" name="telefono" class="input_U" value="" placeholder="Telefono">
                        <i class="input_icon fas fa-mobile-alt"></i>
                    </div>
                </div>
            </div>
            <div class="usuario_item">
                <button class="btn_BCG" type="submit" style="margin: auto; margin-top: 20px; width:100%">
                    <p class="BCGP">Enviar</p>
                </button>
            </div>
        </form>
    </div>
</div>
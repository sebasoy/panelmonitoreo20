<?php
include_once(__DIR__.'/MenuAyuda.php');
?>

<div id="ContenedorOpciones">
    <div class="ContenidoOpcion"  style='cursor: pointer;' onclick="javascript: Mu_Oc('Ayuda1');">
        <div class="ConteOpTitulo">¿Qué es el sistema de monitoreo Smartsen?</div>
        <br>
        <div class="ConteOpText" id="Ayuda1" style="display: none; ">Pixsun desarrolla un monitoreo energético como parte del manejo de los recursos en tu hogar, oficina, comercio o industria. El uso y gestión inteligente de estos recursos implica una buena administración y que se traduce en la disminución de tu consumo. La información en tiempo real junto con datos históricos permite adaptar ese uso a tus necesidades. Este monitoreo que te brinda Pixsun te ayuda a entender lo que sucede con tu sistema de generación FV y tu consumo. Poder visualizar esta información te permitirá eventualmente, tomar decisiones que signifiquen ahorros económicos.
        </div>
        <br>
    </div>



    <div class="ContenidoOpcion" style='cursor: pointer;' onclick="javascript: Mu_Oc('Ayuda2');">
        <div class="ConteOpTitulo">¿Cómo se instala un dispositivo Smartsen?</div>

        <br>

        <div class="ConteOpText" id="Ayuda2" style="display: none; ">Revisá la guía de instrucciones. La conexión es simple y no modifica ni interfiere en los artefactos eléctricos que tengas.</div>
        <br>
    </div>



    <div class="ContenidoOpcion" style='cursor: pointer;' onclick="javascript: Mu_Oc('Ayuda3');">
        <div class="ConteOpTitulo">¿Cómo funciona Smartsen?</div>
        <br>
        <div class="ConteOpText" id="Ayuda3" style="display: none; ">Smartsen es un dispositivo de fácil instalación y operación, que permite monitorear el funcionamiento de tu sistema FV y consumo energético, por medio de la obtención y almacenamiento de diferentes parámetros que se procesan y se pueden visualizar en tiempo real.</div>
        <br>
    </div>




    <div class="ContenidoOpcion" style='cursor: pointer;' onclick="javascript: Mu_Oc('Ayuda4');">
        <div class="ConteOpTitulo">¿Cómo funciona el monitoreo energético Smartsen?</div>

        <br>

        <div class="ConteOpText" id="Ayuda4" style="display: none; ">El sistema mide la energía y genera pautas de control de consumo con notificaciones y gráficos. Podrás saber cómo es tu consumo, como podés ahorrar, como funciona tu instalación FV, etc. El sistema te ayuda a saber que está pasando con tu consumo.</div>
        <br>
    </div>



    <div class="ContenidoOpcion" style='cursor: pointer;' onclick="javascript: Mu_Oc('Ayuda5');">
        <div class="ConteOpTitulo">¿Funciona en 380v trifásico?</div>

        <br>

        <div class="ConteOpText" id="Ayuda5" style="display: none; ">De momento, solo tenemos versión para 220v monofásico.</div>
        <br>
    </div>




    <div class="ContenidoOpcion" style='cursor: pointer;' onclick="javascript: Mu_Oc('Ayuda6');">
        <div class="ConteOpTitulo">Quiero modificar mi contraseña, ¿Cómo hago?</div>

        <br>
        <div class="ConteOpText" id="Ayuda6" style="display: none; ">Ingresa en Mi Cuenta, Datos, Modificar Contraseña.</div>
        <br>
    </div>



    <div class="ContenidoOpcion" style='cursor: pointer;' onclick="javascript: Mu_Oc('Ayuda7');">
        <div class="ConteOpTitulo">Quiero modificar mi vista de la gráfica, ¿Cómo hago?</div>

        <br>

        <div class="ConteOpText" id="Ayuda7" style="display: none; ">Ingresa a Configuración, Monitoreo, Iniciar con detalles históricos.</div>
        <br>
    </div>



    <div class="ContenidoOpcion" style='cursor: pointer;' onclick="javascript: Mu_Oc('Ayuda8');">
        <div class="ConteOpTitulo">¿Qué significa Autoconsumo?</div>

        <br>


        <div class="ConteOpText" id="Ayuda8" style="display: none; ">Definimos autoconsumo a la energía que es consumida en el mismo momento que es generada [autoconsumo instantáneo]. El porcentaje representa toda la energía generada en relación a la consumida.</div>
        <br>
    </div>



    <div class="ContenidoOpcion" style='cursor: pointer;' onclick="javascript: Mu_Oc('Ayuda9');">
        <div class="ConteOpTitulo">Smartsen no puede conectarse al WiFi.</div>

        <br>

        <div class="ConteOpText" id="Ayuda9" style="display: none; ">Verificá que al momento de configurar tu Smartsen hayas seleccionando la red WIFI y la contraseña correcta. Revisá el manuel de instalación para mas detalles.</div>
        <br>
    </div>

</div>

<br><br><br>
    

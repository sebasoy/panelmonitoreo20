<?php
if (session_status() != PHP_SESSION_NONE) {   //
    session_destroy();
}

session_start();

include_once(__DIR__.'/../../modelos/login.class.php');
$login=new login();
if ($login->inicia(3600, $_POST['input_U'], $_POST['input_C'])) {
    if(!$_SESSION['RESET_PASS']){
        header("Location: ../../index.php?P=Monitoreo");
    }else{
        header("Location: ../../index.php?P=MiCuenta&M=MiCu_Contrasena");
    }
} else {
    header("Location: ../../index.php?msj=error_login");
};
$succMsg = 'Your contact request have submitted successfully.';
?>

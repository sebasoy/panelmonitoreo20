<div class="wrap_cbox_by_pixsun">
    <h1>SmartSen By Pixsun</h1>
    <div class="cuerpo_cbox_by_pixsun">
        <div class="cbox_titulos">Acerca de Pixsun</div>
        Somos una empresa de servicios energéticos que desarrolla proyectos de eficiencia energética y energía solar; 
        principalmente fotovoltaica (FV) en el marco de la Ley Nacional 27.424 sobre Generación Distribuida.

        <div class="cbox_titulos">Origen</div>
        La adquisición de un sistema FV para autogeneración define una relación directa con el ahorro y la eficiencia 
        energética. Ambas situaciones están íntimamente vinculadas y conducen al mismo problema. Existe una necesidad de 
        responder a la falta de información o desconocimiento sobre hábitos de consumo o sobre el funcionamiento de una 
        instalación FV.

        <div class="cbox_titulos">Que es SmartSen</div>
        Es un servicio de post-venta para brindarte tranquilidad mediante una plataforma de seguimiento y asistencias 
        inteligentes para corregir o mejorar ciertos hábitos consumo energético. De este modo podrás ahorrar electricidad 
        y, de obtener el máximo provecho de tu propia generación.<br>
        Es una herramienta que te brinda información en tiempo real para que puedas entender lo que está sucediendo con tu 
        sistema de generación FV y el consumo de energía eléctrica en tu vivienda, oficina o industria. Visualizar estos 
        datos de manera simple y rápida te permite, eventualmente, tomar decisiones que signifiquen ahorros económicos para 
        tu bolsillo. El uso y gestión apropiado de estos recursos, junto al análisis de datos históricos; facilita que 
        puedas adaptar tus necesidades de un modo más consciente y eficiente.

        <div class="cbox_titulos">Objetivos y Beneficios</div>
        El principal objetivo de Smartsen es lograr traducir toda la información eléctrica relacionada a tu generación y
        consumo.
        Entre otros te permite:
        • Empoderarte mediante el acceso a información y brindarte la posibilidad de convertirte en un sujeto activo del 
        mercado energético, con capacidad de entender y actuar consecuentemente.
        • Ofrecerte tranquilidad por decidir instalar un sistema FV para autoconsumo.
        • Entender como es tu consumo eléctrico, brindándote herramientas de control y gestión para su uso más
        consciente y eficiente. Ayudarte a corregir tus hábitos de consumo energéticos para obtener un ahorro energético.
        • Crear una plataforma que propague los beneficios ambientales de la Generación de energía eléctrica Distribuida mediante fuentes renovables.
        • Conocer los datos de generación del sistema FV y como está funcionando.
        • Monitorear en tiempo real el consumo e integrarlo con tus datos históricos para analizar cuáles y como son
        tus tendencias o desvíos.

        <div class="cbox_titulos">Nuestro Equipo</div>
        Somos un equipo multidisciplinar con experiencia en gestión y desarrollo de proyectos fotovoltaicos. Estamos convencidos 
        que la energía solar, y la Generación Distribuida (generar energía en el lugar donde es consumida), junto al empoderamiento ciudadano es
        el camino hacia la transformación energética.

        <div class="cbox_titulos">Comunidad Solar Pixsun</div>
        Ayúdanos a ampliar la Comunidad Solar Pixsun para que cada vez seamos más los que estamos comprometidos con la lucha 
        contra el Cambio Climático, entendiendo que uno de los principales factores en las emisiones de Gases Efecto Invernadero 
        [GEI], radica en la generación de energía. Sin dudas, al generar nuestra propia energía de un modo amigable con el 
        ambiente aportamos lo nuestro.
    </div>
</div>
<?php
//chequeo si esta logueado
if (!isset($_SESSION['ID_SISTEMA'])) {
    if ($_GET['msj'] == 'error_login') {
        echo "<div class='wrap_msj'>
        <span class='texto_msj'>Usuario o contraseña incorrecta.</span>
        <i class='x_cerrar fas fa-times'></i>
        </div>";
    }
    if ($_GET['msj'] == 'mail_enviado') {
        echo "<div class='wrap_msj'>
        <span class='texto_msj'>Un email fue enviado a su casilla de correo.</span>
        <i class='x_cerrar fas fa-times'></i>
        </div>";
    }
    if ($_GET['msj'] == 'mail_no_enviado') {
        echo "<div class='wrap_msj'>
        <span class='texto_msj'>Ocurrio un error al enviar el mail.</span>
        <i class='x_cerrar fas fa-times'></i>
        </div>";
    }

    if (!isset($_GET['M'])) {
        ?>
<div id="contenedor_LOG">
    <form name="frm_login" method="post" action="vistas/sitio/login.php">
        <?php //echo $U." - ".$P." - ".$I." - ".$U." - ".$_SESSION['ID_SISTEMA'];
                //echo $_SESSION['idusuario']." - ".$_SESSION['ID_SISTEMA']." - ".$_SESSION['PAGE'];?>
        <h1 class="bienvenid-a-cbox-sm">Bienvenid@ a CBOX, tu plataforma de monitoreo energético</h1>
        <br><br>

        <p class="para-ingresar-al-sis">Para ingresar por primera vez, busca la contraseña en las instrucciones.</p>

        <p class="usuario">Usuario</p>
        <div style="width: auto">
            <input type="text" class="input_U" name="input_U" value="" placeholder="Usuario">
            <i class="input_icon fas fa-user-alt"></i>
        </div>
        <p class="usuario">Contrase&ntilde;a</p>
        <div style="width: auto">
            <input type="password" class="input_C" name="input_C" placeholder="Contraseña">
            <i class="input_icon fas fa-lock"></i>
        </div>
        <div id="Mensaje1">
            <div class="Izq">
                <p><a href="?M=Olvido_Contrasena" class="olvidaste-tu-contra">¿Olvidaste tu Contrase&ntilde;a?</a></p>
            </div>
            <!--<div class="Der">
                <p><a href="#" class="recordar-usuario">Recordar usuario</a></p>
            </div>
            <div class="Der_Mrg"><img src="Img/Check.png" width="15" height="15" alt="" /></div>-->
        </div>
        <br> 
        <button class="btn" type="submit">
            <p class="ingresar">Ingresar</p>
        </button>
       <!-- <div class="g-recaptcha" data-sitekey="6Le0mpQUAAAAAHntvx1H5zCKAGAKgzwt0miJSWHC"> -->
    </form>
</div>
<?php
    }
    if ($_GET['M'] == 'Olvido_Contrasena') {
        include_once(__DIR__.'/Olvido_Contrasena.php');
    }
    //esta logueado ya
} else {

    header("Location: index.php?P=Configuracion");
}

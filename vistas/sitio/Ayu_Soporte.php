<?php
include_once(__DIR__.'/MenuAyuda.php');
?>

<div class="wrap_cbox_by_pixsun">
    <div class="cuerpo_cbox_by_pixsun">
        <form method="post" action="vistas/sitio/Ayu_Soporte_success.php" style="max-width: 945px">
            <div class="" style="display:flex;flex-direction: column">
                <div class="usuario_item">
                    <label for="tema">Tema </label>
                    <select name="tema" id="tema">
                        <option value="panel">Ayuda sobre el funcionamiento del sistema fotovoltaico</option>
                        <option value="consumo">Ayuda sobre el sistema de monitoreo</option>
                        <option value="monitoreo">Ayuda sobre datos de la cuenta</option>
                        <option value="otros">Ayuda sobre datos de la instalación</option>
                        <option value="otros">Otro</option>
                    </select>
                </div>
                <div class="usuario_item">
                    <label for="mensaje">Mensaje </label>
                    <textarea class="input_U" name="mensaje" rows="4" cols="50" placeholder="Escriba su mensaje" required></textarea>
                </div>
            </div>
            <div class="usuario_item">
                <button class="btn_BCG" type="submit" style="margin: auto; margin-top: 20px; width:100%">
                    <p class="BCGP">Enviar</p>
                </button>
            </div>
        </form>
    </div>
</div>
<div class="Contenedor_Menu">
    <div class="rectangle-2" style="margin-bottom: 1px;">
        <p class="cbox-smart-monitorin">Smartsen<br>Monitoreo Energético</p>
    </div>


    <?php error_reporting(0); ?>
    <?php $logeado = isset($_SESSION['ID_SISTEMA']) ? true : false;  ?>


    <div class="rectangle-2-40R" <?php echo $Sel[1]; ?>>
        <img src="Img/menu/monitoreo-icon.png" class="monitoreo-icon" />
        <a class="ItemMenu" href="<?= $logeado ? '?P=Monitoreo' : '?P=Logueo'; ?>">Monitoreo</a>
    </div>


    <div class="rectangle-2-40R" <?php echo $Sel[2]; ?>>
        <img src="Img/menu/configuracion-icon.png" class="configuracion-icon" />
        <a class="ItemMenu" href="<?= $logeado ? '?P=Configuracion' : '?P=Logueo'; ?>">Configuración</a>
    </div>

    <div class="rectangle-2-40R" <?php echo $Sel[3]; ?>>
        <img src="Img/menu/user-icon.png" class="micuenta-icon" />
        <a class="ItemMenu" href="<?= $logeado ? '?P=MiCuenta' : '?P=Logueo'; ?>">Mi
            Cuenta</a>
    </div>
    <?php  if ($_GET['P'] != "Logout") {
        ?>
        <div class="rectangle-2-40R" <?php echo $Sel[4]; ?>>
            <!--<img src="Img/menu/user-icon.png" class="micuenta-icon" />-->
            <!-- <i class="fas fa-user-alt-slash" style="margin-left: 15px;"></i>
             <a class="ItemMenu" href="?P=Logout" style="margin-left: 7px;">Logout</a> -->
        </div>

        <?php
    } ?>
</div>


<div class="Contenedor_Menu" style="align-self: flex-end">
    <div class="rectangle-2-40R" <?php echo $Sel[4]; ?>>
        <img src="Img/menu/Compartir-icon.png" class="compartir-icon" />
        <a class="ItemMenu" href="?P=Recomendar">Recomendar</a>
    </div>
    <div class="rectangle-2-40R" <?php echo $Sel[5]; ?>>
        <img src="Img/menu/ayuda-icon.png" class="ayuda-icon" />
        <a class="ItemMenu" href="?P=Ayuda">Ayuda</a>
    </div>
    <div class="rectangle-2-40R" <?php echo $Sel[6]; ?>>
        <img src="Img/menu/pixsun-icon.png" class="fill-8" />
        <a class="ItemMenu" href="?P=Pixsun">CBOX by Pixsun</a>
    </div>

    <?php
    //////////////////// ESTADO DE LA CONEXION  //////////////////////////
    //devuelve la ultima entrada en los ultimos 15min.
    require_once('Estado.php');
    //$sesion=0;
    //$estado = 'OK';
    if ($estado=='') {
        ?>
        <div class="rectangle-2-60" style="">
            <p class="estado-del-cbox">Estado del CBOX</p>
            <div id="OVAL" class="oval-0">&nbsp;</div>
            <div class="al-lado">
                <p id="CONEXION" class="Verif">No conectado</p>
            </div>
        </div>
        <?php
    } elseif ($estado=='OK') {
        ?>
        <div class="rectangle-2-60" style="	border-left: 4px solid #00CA9D;margin-top: 20px; ">
            <p class="estado-del-cbox">Estado del CBOX</p>
            <div id="OVAL" class="oval-1">&nbsp;</div>
            <div class="al-lado">
                <p id="CONEXION" class="conectado-ok">Conectado Ok</p>
            </div>
        </div>
        <?php
    } elseif ($estado=='') {
        ?>
        <div class="rectangle-2-60" style="	border-left: 4px solid #FF435A;margin-top: 20px; ">
            <p class="estado-del-cbox">Estado del CBOX</p>
            <div id="OVAL" class="oval-2">&nbsp;</div>
            <div class="al-lado">
                <p id="CONEXION" class="Verif">Verificando</p>
            </div>
        </div>
        <?php
    } elseif ($estado == 'TEST') {
        ?>
        <div class="rectangle-2-60" style="	border-left: 4px solid #F8E71C;margin-top: 20px; ">
            <p class="estado-del-cbox">Estado del CBOX</p>
            <div id="OVAL" class="oval-3">&nbsp;</div>
            <div class="al-lado">
                <p id="CONEXION" class="configestado">Configuración</p>
            </div>
        </div>
        <?php
    } ?>
</div>
</div>
<?php
include_once(__DIR__.'/MenuMiCuen.php');
include_once(__DIR__.'/DatosUser.php');
?>
<form name="frm_login" method="post" action="vistas/MiCuenta/MiCu_Datos_success.php">
    <div class="wrap_datos_usuario usuario">
        <div class="usuario_item">
            <label for="nombre">Nombre y Apellido</label>
            <div style="width: auto">
                <input type="text" name="nombre" class="input_U" value="<?php echo $N; ?>">
                <i class="input_icon fas fa-user-alt"></i>
            </div>
        </div>
        <div class="usuario_item">
            <label for="telefono">Tel&eacute;fono</label>
            <div style="width: auto">
                <input type="text" name="telefono" class="input_U" value="<?php echo $T; ?>">
                <i class="input_icon fas fa-phone"></i>
            </div>
        </div>
        <div class="usuario_item">
            <label for="direccion">Direcci&oacute;n de Instalaci&oacute;n</label>
            <div style="width: auto">
                <input type="text" name="direccion" class="input_U" value="<?php echo $D; ?>">
                <i class="input_icon fas fa-home"></i>
            </div>
        </div>
        <div class="usuario_item">
            <label for="provincia">Provincia</label>
            <select name="provincia" size="1" id="">
                <?php
            for ($i=0; $i < count($Provincias); $i++) {
                if ($i == $P) {
                    $selec=" selected ";
                } else {
                    $selec="";
                }
                echo '<option value="'.$i.'" '.$selec.' >'.$Provincias[$i].'</option>';
            }?>
            </select>
        </div>
        <div class="usuario_item" style="width: 100%">
            <h3 class="">Notificaciones y Alertas</h3>
        </div>
        <div class="usuario_item">
            <label for="celular">Celular para recibir alertas</label>
            <div style="width: auto">
                <input type="text" name="celular" class="input_U" value="<?php echo $Tr; ?>">
                <i class="input_icon fas fa-mobile-alt"></i>
            </div>
        </div>
        <div class="usuario_item">
            <label for="mail">Mail para recibir reportes</label>
            <div style="width: auto">
                <input type="email" name="mail" class="input_U" value="<?php echo $Er; ?>">
                <i class="input_icon fas fa-envelope"></i>
            </div>
        </div>

        <div class="usuario_item">
            <button class="btn_BCG" type="submit" style="margin: auto; margin-top: 20px">
                <p class="BCGP">Guardar</p>
            </button>
        </div>
    </div </form> <!--<div id="ContenedorOpciones">
    <div class="ContenidoOpcion_Datos">
        <div class="ConteOpText">

            <p class="usuario">Usuario</p>
            <input type="text" name="usuario" class="input_U" value="davidcohen@gmail.com">

        </div>
    </div>

    <div class="ContenidoOpcion_Datos">
        <div class="ConteOpText">

            <p class="usuario">Contrase&ntilde;a</p>
            <input type="password" class="input_U" value="davidcohen@gmail.com">


        </div>
    </div>

    </div>

    <br><br><br>

    <div id="ContenedorOpciones">
        <div class="ContenidoOpcion_Datos">
            <div class="ConteOpText">

                <p class="usuario">Nombre y Apellido</p>
                <input type="text" name="nombre" class="input_U" value="<?php echo $N; ?>">

            </div>
        </div>
        <div class="ContenidoOpcion_Datos">
            <div class="ConteOpText">

                <p class="usuario">Tel&eacute;fono</p>
                <input type="text" name="telefono" class="input_U" value="<?php echo $T; ?>">

            </div>
        </div>
    </div>

    <br><br><br>

    <div id="ContenedorOpciones">
        <div class="ContenidoOpcion_Datos">
            <div class="ConteOpText">

                <p class="usuario">Direcci&oacute;n de Instalaci&oacute;n</p>
                <input type="text" class="input_U" value="<?php echo $D; ?>">

            </div>
        </div>
        <div class="ContenidoOpcion_Datos">
            <div class="ConteOpText">

                <p class="usuario">Provincia</p>

                <select name="Provincia" size="1" id="">
                    <?php for ($i=0; $i < count($Provincias); $i++) {
                if ($Provincias[$i]==$P) {
                    $selec=" selected ";
                } else {
                    $selec="";
                }
                echo '<option value="'.$i.'" '.$selec.' >'.$Provincias[$i].'</option>';
            }?>
                </select>


            </div>
        </div>
    </div>

    <br><br><br>

    <div style="width:100%; height:150px;">
        <p class="notificaciones-y-ale">Notificaciones y Alertas</p>
    </div>
    <div id="ContenedorOpciones">
        <div class="ContenidoOpcion_Datos">
            <div class="ConteOpText">
                <p class="usuario">Celular para recibir alertas</p>
                <input type="text" class="input_U" value="<?php echo $Tr; ?>">
            </div>
        </div>
        <div class="ContenidoOpcion_Datos">
            <div class="ConteOpText">
                <p class="usuario">Mail para recibir reportes</p>
                <input type="text" class="input_U" value="<?php echo $Er; ?>">
            </div>
        </div>
    </div>
    <div id="BotonGuardar">
        <div class="BotConfGuardar">
            <button class="btn_BCG" type="submit">
                <p class="BCGP">Guardar</p>
            </button></div>
        <div class="BotConfCancelar"><a>Cancelar</a></div>
    </div>-->
<?php
require_once __DIR__."/../../modelos/db.class.php";

//MENSAJES ACTUALIZAR INFORMACION DE CONTACTO
if (isset($_GET['msj'])) {
    switch ($_GET['msj']) {
        case 'actualizado':
            $msj = '¡Informacion actualizada!';
            break;
        case 'success':
            $msj = '¡Informacion actualizada!';
            break;
        case 'vacio':
            $msj = 'Uno de los campos esta vacio!';
            break;
        case 'desiguales':
            $msj = 'La nueva contraseña no coincide!';
            break;
        case 'bad_pass':
            $msj = 'Contraseña actual incorrecta!';
            break;
        case 'desiguales':
            $msj = 'La nueva contraseña no coincide!';
            break;
        case 'pass_success':
            $msj = 'Contraseña actualizada!';
            break;
        case 'pass_error':
            $msj = 'Error al actualizar la Contraseña, intentelo nuevamente.';
            break;
        default:
            $msj = $_GET['msj'];
            break;
    }
    echo "<div class='wrap_msj'>
            <span class='texto_msj'>".$msj."</span>
            <i class='x_cerrar fas fa-times'></i>
        </div>";
}

if ($_GET['M']=="" or !isset($_GET['M']) or $_GET['M']=="MiCuenta_Datos") {
    $SelC[1]=$Style_MC;
    include_once(__DIR__.'/MiCu_Datos.php');
} elseif ($_GET['M']=="MiCuenta_Documentos") {
    $SelC[2]=$Style_MC;
    include_once(__DIR__.'/MiCu_Documentos.php');
} elseif ($_GET['M']=="MiCu_Contrasena") {
    $SelC[3]=$Style_MC;
    include_once(__DIR__.'/MiCu_Contrasena.php');
}

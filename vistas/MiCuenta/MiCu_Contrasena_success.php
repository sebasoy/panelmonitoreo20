<?php

session_start();
if (empty($_POST['input_U']) || empty($_POST['input_C'])) {
    header("Location: ../../index.php?P=MiCuenta&M=MiCu_Contrasena&msj=vacio");
} elseif ($_POST['input_U'] != $_POST['input_C']) {
    header("Location: ../../index.php?P=MiCuenta&M=MiCu_Contrasena&msj=desiguales");
} else {
    require "../../modelos/db.class.php";
    $db = new db\db\db();
    $conn = $db->connectDB();
    //regeneor la contraseña
    $query_pass = "SELECT * FROM Usuarios WHERE USER = '" . $_SESSION['USER_NAME'] . "' LIMIT 1";
    $pass_actual = $_POST['input_A'];

    $result = mysqli_query($conn, $query_pass);
    while ($fila = mysqli_fetch_assoc($result)) {
        $P = $fila['PASS'];
    }

    if (!password_verify($pass_actual, $P)) {
        header("Location: ../../index.php?P=MiCuenta&M=MiCu_Contrasena&msj=bad_pass");
    } else {
        $passHash = password_hash($_POST['input_U'], PASSWORD_DEFAULT);
        //$query = "UPDATE usuarios SET PASS = '".$hash."', COMENT = '".$random."' WHERE ID = ".$_SESSION['ID_USER']." LIMIT 1";
        $query = "UPDATE Usuarios SET PASS = '" . $passHash . "', ResetPass = 0  WHERE ID = ". $_SESSION['ID_USER'] . " LIMIT 1";

        //var_dump($query);
        try {
            $result = mysqli_query($conn, $query);
            header("Location: ../../index.php?P=MiCuenta&M=MiCu_Contrasena&msj=pass_success");
        } catch (Exception $e) {
            //echo 'Message: ' . $e->getMessage();
            header("Location: ../../index.php?P=MiCuenta&M=MiCu_Contrasena&msj=pass_error");

        }
    }
}

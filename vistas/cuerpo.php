<body>
<?php //error_reporting(E_ALL); ?>
<div id="Menu_Lateral">
    <?php include(__DIR__."/menu.php"); ?>
</div>

<?php if (isset($_SESSION['ID_SISTEMA'])) {   ?>
    <div id="Header-User">
        <?php include("Header-User.php");?>
    </div>
<?php }else{  ?>
    <div id="Header-User">
        <?php include("Header-User-Logout.php");?>
    </div>
<?php }  ?>



<div id="marcos">
    <div id="contenedor">
        <?php
        $Sel = array();
        $Sel_C = array();
        $Sel_M = array();
        $Sel_MA = array();
        $Style_ML = ' style="background-color: #3A3F51; border-left: 4px solid #00CA9D;" ';
        $Style_MC = ' style="border-bottom: 3px solid #6980E4; color: #3A3F51; font-weight: bold; " ';
        $Style_MM = ' style="border-radius: 24px;  background-color: #FFFFFF; box-shadow: 0 1px 3px 0 rgba(0,0,0,0.12), 0 1px 6px 0 rgba(0,0,0,0.03); " ';
        $Style_MMA = ' style="color: #697B8C; " ';

        if ($_GET['P'] == "Logout") {
            $_GET['P'] = "";
            include_once(__DIR__.'/sitio/logueo.php');
        } elseif ($_GET['P'] == "Logueo") {
            include_once(__DIR__.'/sitio/logueo.php');
        } elseif ($_GET['P'] == "Monitoreo") {
            $Sel[1] = $Style_ML;
            include_once(__DIR__.'/Monitoreo/Monitoreo.php');
        } elseif ($_GET['P'] == "Configuracion") {
            $Sel[2] = $Style_ML;
            include_once(__DIR__.'/Configuracion/Configuracion.php');
        } elseif ($_GET['P'] == "MiCuenta") {
            $Sel[3] = $Style_ML;
            include_once(__DIR__.'/MiCuenta/MiCuenta.php');
        } elseif ($_GET['P'] == "Recomendar") {
            $Sel[4] = $Style_ML;
            include_once(__DIR__.'/sitio/Recomendar.php');
        } elseif ($_GET['P'] == "Ayuda") {
            $Sel[5] = $Style_ML;
            include_once(__DIR__.'/sitio/Ayuda.php');
        } elseif ($_GET['P'] == "Pixsun") {
            $Sel[6] = $Style_ML;
            include_once(__DIR__.'/sitio/Pixsun.php');
        } elseif (!isset($_GET['P'])) {
            if (!isset($_SESSION['ID_SISTEMA'])) {
                include_once(__DIR__.'/sitio/logueo.php');
            } else {
                $Sel[2] = $Style_ML;
                include_once(__DIR__.'/Configuracion/Configuracion.php');
            }
        }

        ?>
    </div>
</div>
<div id="Mensaje2">
    <p class="su-nueva-contrasena">¡Su nueva contraseña ha sido generada con &eacute;xito! <a href="#" class="btn_x">X</a></p>
</div>
</body>
</html>


<script type="text/javascript">
    /*$("#enlace").click(function() {
        location.href = this.href; // ir al link
    });*/
    // lanzamos la llamada al evento click
    //$("#enlace").click();

</script>
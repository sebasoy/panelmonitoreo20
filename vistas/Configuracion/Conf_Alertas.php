<?php
require_once __DIR__.'/MenuConfig.php';
require_once __DIR__.'/DatosConfig.php';
require_once __DIR__.'/Botonin.php';
?>


<!-- configuracion avanzada

  <div id="ContenedorOpciones" style="text-align:right;">
  <a href="javascript:void(0);" onclick="javascript: Mu_Oc('Afch');">Configuraci&oacute;n Avanzada </a>
  </div>

-->
<div class="disabled">
    <div id="ContenedorOpciones" >
        <div class="Tecla"><?php BotonI("Afc"); ?></div>
        <div class="ContenidoOpcion">
            <div class="ConteOpTitulo">Falla de conexión</div>

            <div class="configuracion-avanza" >
                <a href="javascript:void(0);" onclick="javascript: Mu_Oc('Afch');">Configuraci&oacute;n Avanzada </a>
            </div>


            <div class="ConteOpText">Al activar esta opción, te avisaremos en la <a href="">direccion configurada</a> si CBOX no se puede comunicar.
            </div>

            <div class="ConteOpText" id="Afch" style="display:none;" ><b>Tiempo de desconexión </b>
                <?php BotonS("Afch", "SelecHoras"); ?> Horas.
            </div>

        </div>
    </div>


    <!--
    <div id="path-2-copy"></div> -->

    <div id="ContenedorOpciones">
        <div class="Tecla"><?php BotonI("Afm"); ?></div>
        <div class="ContenidoOpcion">
            <div class="ConteOpTitulo">Falla en la medición</div>
            <div class="ConteOpText">En caso que alguno de los sensores no esté funcionando correctamente, recibirás un aviso a la <a href="">direccion configurada</a>.</div>
        </div>
    </div>

    <!--
    <div id="path-2-copy"></div> -->


    <div id="ContenedorOpciones">
        <div class="Tecla"><?php BotonI("Aabt"); ?></div>
        <div class="ContenidoOpcion">
            <div class="ConteOpTitulo">Alta/baja tensión</div>
            <div class="configuracion-avanza" >
                <a href="javascript:void(0);" onclick="javascript: Mu_Oc('ContenidoOpcion');">Configuraci&oacute;n Avanzada </a>
            </div>

            <div class="ConteOpText">Si la tensión eléctrica estuviera fuera de los parámetros establecidos, recibirás un aviso a la  <a href="">direccion configurada</a>.</div>
        </div>

        <div id="ContenidoOpcion" style="margin-left: 0px; padding-left:100px; display:none;" >

            <div class="ContOpc3C">
                <div class="ConteOpText">
                    <b>L&iacute;mite superior </b>
                    <p class="RangoDesplegable">Rango entre 221v y 250v.</p>
                    <?php BotonS("Aabtls", "SelecRango"); ?> Voltios.
                </div>
            </div>

            <div class="ContOpc3C" >
                <div class="ConteOpText">
                    <b>L&iacute;mite Inferior </b><br>
                    <p class="RangoDesplegable">Rango entre 160v y 219v.</p>
                    <?php BotonS("Aabtli", "SelecRango2"); ?> Voltios.
                </div>
            </div>

            <div class="ContOpc3C">
                <!-- (!) -->
            </div>
        </div>
    </div>
</div>
<?php if ($_GET['C']=="Conf_Alertas") {
    ?>
    <h1 class="TiConfig">Configuraci&oacute;n</h1>
    <!--<p><a href="?P=Configuracion&C=Conf_SFV" class="configuracion-sfv" >Configuraci&oacute;n SFV</a></p> -->
    <?php
} elseif ($_GET['C']=="Conf_SFV") {
    ?>
    <h1 class="TiConfig">Configuraci&oacute;n SFV</h1>
    <!--<p><a href="?P=Configuracion&C=Conf_Alertas" class="configuracion-sfv" >< Volver a configuraci&oacute;n</a></p> -->
    <?php
} else {
    ?>
    <h1 class="TiConfig">Configuraci&oacute;n</h1>
    <?php
} ?>

<div id="MeConfig">
    <div class="ItMenuConfig">
        <p>
            <a href="?P=Configuracion&C=Conf_Monitoreo" <?php echo $SelC[1]; ?>>
                Monitoreo</a>
        </p>
    </div>

    <div class="ItMenuConfig">
        <p>
            <a  href="<?php echo '?P=Configuracion&C=Conf_Notificaciones'?>"
                <?= $SelC[2]; ?>>
                Notificaciones</a>
        </p>
    </div>

    <div class="ItMenuConfig ">
        <p>
            <a  href="<?php echo '?P=Configuracion&C=Conf_Alertas'?>"
                <?= $SelC[3]; ?>>
                Alertas</a>
        </p>
    </div>

    <?php if($rol == 1) { ?>
        <div class="ItMenuConfig">
            <p>
                <a href="?P=Configuracion&C=Conf_Consumo" <?php echo $SelC[4]; ?>>
                    Consumo</a>
            </p>
        </div>
    <?php } ?>
    <?php if($rol == 1) { ?>
        <div class="ItMenuConfig">
            <p>
                <a href="?P=Configuracion&C=Conf_SFV" <?php echo $SelC[5]; ?>>
                    Configuración SFV</a>
            </p>
        </div>
    <?php } ?>

</div>


<!-- class="disabled-mvp" estaba en la pestaña de alertas y notificaciones-->
<?php
include_once(__DIR__.'/MenuConfig.php');
include_once(__DIR__.'/DatosConfig.php');
include_once(__DIR__.'/Botonin.php');
?>

<div class="disabled">
    <div id="ContenedorOpciones">
        <div class="Tecla">
            <?php BotonI("Nerm"); ?>
        </div>
        <div class="ContenidoOpcion">
            <div class="ConteOpTitulo">Envio de reporte</div>
            <div class="ConteOpText">Al activar esta opción, recibirás un reporte periódico en la
                <a href="?P=MiCuenta">dirección
                    configurada</a>.</div>

            <div class="ConteOpText">
                <?= BotonS("Nermp", "SelecMensual"); ?>
            </div>

            <div class="">
                <div class="Tecla_MiCuenta">
                    <?php BotonC("Nermrra"); ?>
                </div>
                <p style="color: #697B8C;">Recibir reporte avanzado
                    <i class="fas fa-question-circle" title="Si tu cuenta lo permite, recibirás en los reportes datos y estadísticas avanzadas sobre el consumo que estás teniendo."
                       style="font-weight: 900;font-size: 16px;color: #0904e8ad;"></i>
                </p>
            </div>
        </div>

    </div>

    <br><br><br>
    <div id="ContenedorOpciones">
        <div class="Tecla">
            <?php BotonI("Nelcm"); ?>
        </div>
        <div class="ContenidoOpcion">
            <div class="ConteOpTitulo">Envío de límite de consumo</div>
            <div class="ConteOpText">Al activar esta opción, te avisaremos cuando estés cerca de alcanzar
                tu límite de consumo. a la <a href="">dirección configurada</a>.</div>
        </div>
    </div>

    <br><br><br>


    <div id="ContenedorOpciones">
        <div class="Tecla">
            <?php BotonI("Nenoac"); ?>
        </div>
        <div class="ContenidoOpcion">
            <div class="ConteOpTitulo">Envió de notificaciones de Ahorro</div>
            <div class="ConteOpText">Nos basamos en tus consumos anteriores para ayudarte a alcanzar el ahorro esperado.
                Esto lo podrás ver en la ventana de monitoreo y recibirás notificaciones en la
                <a href="">dirección configurada</a>.</div>
        </div>
    </div>

    <br><br><br>


    <div id="ContenedorOpciones">
        <div class="Tecla">
            <?php BotonI("Nencfe"); ?>
        </div>
        <div class="ContenidoOpcion">
            <div class="ConteOpTitulo">Envío de notificación por consumo fuera de lo esperado</div>
            <div class="ConteOpText">Si detectamos que hay un exceso en el consumo fuera de lo normal o habitual,
                te avisamos para que lo tengas en cuenta y puedas actuar en consecuencia.</div>
        </div>
    </div>


    <br><br><br>
</div>
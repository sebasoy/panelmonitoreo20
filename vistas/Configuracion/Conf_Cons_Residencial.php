<!-- ///////////////////////// opcion residencial /////////////////////  -->
<div class="ContOpc3C">
    <div class="ConteOpText">
        <b>Tipo de vivienda</b><br>
        <?php BotonR2("Ctv", "Casa", "Departamento"); ?>
    </div>
</div>
<br>
<div class="ContOpc3C">
    <div class="ConteOpText">
        <b>Habitantes</b><br>
        <p>
            <input type="text" class="input_Ca" name="Cha" value="<?= isset($_SESSION["Cha"]) ? $_SESSION["Cha"] : '0' ; ?>">
            Adultos
        </p>
        <p>
            <input type="text" class="input_Ca" name="Chn" value="<?= isset($_SESSION["Chn"]) ? $_SESSION["Chn"] : '0' ; ?>">
            Niños
        </p>
        <p>
            <input type="text" class="input_Ca" name="Chh" value="<?= isset($_SESSION["Chh"]) ? $_SESSION["Chh"] : '0' ; ?>">
            Habitaciones
        </p>
    </div>
</div>
<br>
<div class="ContOpc3C">
    <div class="ConteOpText">
        <b>Precio de la energía</b><img src="Img/pregunta-icon.png" width="24" height="24" /><br>
        <p>
            <input type="text" class="input_Ca" name="Cpe" value="<?php echo number_format($_SESSION["Cpe"], 2); ?>">
            $/kWh
        </p>
    </div>
</div>
<br>
<?php

//actualiza los campos de las VISTAS DE CONFIGURACION
if (isset($_POST['enviado']) and $_POST['enviado']=="OK") {
    unset($_POST['enviado']);
    if (isset($_POST['conf_monitoreo'])) {
        $conf_monitoreo = true;
        unset($_POST['conf_monitoreo']);
    }
    $GuardaHoras="NO";
    $GuardaArtElec="NO";

    foreach ($_POST as $campo => $valor) {
        $CampDiv=explode("-", $campo);
        $CampID=$CampDiv[0];
        //var_dump($CampID);

        //print_r($CampDiv);

        if ($CampDiv[1]=="Cch_H1" or $CampDiv[1]=="Cch_H2" or $CampDiv[1]=="Cch_LU" or $CampDiv[1]=="Cch_MA" or $CampDiv[1]=="Cch_MI" or $CampDiv[1]=="Cch_JU" or  $CampDiv[1]=="Cch_VI" or $CampDiv[1]=="Cch_SA" or $CampDiv[1]=="Cch_DO") {
            $GuardaHoras="SI";
            $SET_Camp_Horas[$CampID].=" $CampDiv[1] = '$valor',";
        } elseif ($CampDiv[1]=="CcAE_Eq" or $CampDiv[1]=="CcAE_T" or $CampDiv[1]=="CcAE_Fc" or $CampDiv[1]=="CcAE_Cg" or $CampDiv[1]=="CcAE_P" or $CampDiv[1]=="CcAE_N") {
            $GuardaArtElec="SI";
            $SET_Camp_ArtElec[$CampID].=" $CampDiv[1] = '".trim($valor)."',";
        } else {
            $SET_Camp.=" $campo = '$valor',";
        }
    }
    //var_dump($SET_Camp_ArtElec);

    $db = new db\db\db();
    $conn = $db->connectDB();

    $sql = "UPDATE Configuracion SET ".trim($SET_Camp, ',')." WHERE IDSISTEMA='".$_SESSION['ID_SISTEMA']."'";

    try {
        mysqli_query($conn, $sql);
        //$var = 'SFV';
    } catch (\Throwable $e) {
        var_dump($sql);
        $e->getMessage();
    } catch (\Exception $e) {
        var_dump($sql);
        $e->getMessage();
    }
    if ($GuardaHoras=="SI") {
        //$var = 'GuardaHoras';
        foreach ($_SESSION["CargaHoraria"] as $clave => $valor) {
            $sqlH = "UPDATE Conf_Horas SET ".trim($SET_Camp_Horas[$clave], ',')." WHERE ID='".$clave."'";
            try {
                mysqli_query($conn, $sqlH);
            } catch (\Throwable $e) {
                var_dump($sqlH);
                $e->getMessage();
            } catch (\Exception $e) {
                var_dump($sqlH);
                $e->getMessage();
            }
        }
    }

    if ($GuardaArtElec=="SI") {
        $var = 'GuardaArtElec';
        //var_dump($_SESSION["ArtElec"]);
        foreach ($_SESSION["ArtElec"] as $clave => $valor) {
            //var_dump($clave);
            $sqlAe = "UPDATE conf_artelec SET ".trim($SET_Camp_ArtElec[$clave], ',')." WHERE ID='".$clave."'";
            try {
                mysqli_query($conn, $sqlAe);
            } catch (\Throwable $e) {
                var_dump($sqlAe);
                $e->getMessage();
            } catch (\Exception $e) {
                var_dump($sqlAe);
                $e->getMessage();
            }
        }
    }
    $_GET['msj'] = 'actualizado';
}

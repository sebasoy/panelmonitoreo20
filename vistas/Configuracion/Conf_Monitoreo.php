<?php
include_once(__DIR__.'/MenuConfig.php');
//DatosConfig trae todos los valores de configuracion y 
//  los guarda en variables de session con clave igual al nombre de la variable
include_once(__DIR__.'/DatosConfig.php');
include_once(__DIR__.'/Botonin.php');
?>

<div id="ContenedorOpciones">
    <!-- BotonI('Midh') hace el funcionamiento del boton, para que se vea en SI o en NO -->
    <div class="Tecla" onclick="$('#id_form').submit();"><?php BotonI("Midh"); ?>
    </div>
    <div class="ContenidoOpcion">
        <div class="ConteOpTitulo">Iniciar con gráficos desplegados</div>
        <div class="ConteOpText">El panel principal se inicia colapsando los gráficos de generación y consumo. Al
            activar esta opción te aparecerán siempre desplegados.</div>
    </div>
</div>


<div id="ContenedorOpciones">
    <!-- BotonI('Miia') hace el funcionamiento del boton, para que se vea en SI o en NO -->
    <div class="Tecla" onclick="$('#id_form').submit();"><?php BotonI("Miia"); ?>
    </div>
    <div class="ContenidoOpcion">
        <div class="ConteOpTitulo">Incluir información avanza</div>
        <div class="ConteOpText">Al activar esta opción, podrás visualizar debajo de las gráficas otros datos
            específicos.</div>
    </div>
</div>

<?php
require_once __DIR__.'/MenuConfig.php';
require_once __DIR__.'/DatosConfig.php';
require_once __DIR__.'/Botonin.php';
require_once __DIR__.'/DatosArtElec.php';

?>


<div id="ContenidoOpcion" style="margin-left: 0px;">
    <div class="ContOpc100">
        <div class="ConteOpText">
            <b>Usuario</b>
            <?php BotonS("Cu", "SelecUsuario");?>
        </div>
    </div>

    <?php
    if ($_SESSION["Cu"] == 1) {
        include_once(__DIR__.'/Conf_Cons_Residencial.php');
    } elseif ($_SESSION["Cu"] == 2) {
        include_once(__DIR__.'/Conf_Cons_Comercial.php');
    }
    ?>
</div>

<!-- fin opciones -->

<hr class="hr" />

<h1 class="TiConfig">Consumos anteriores</h1>

<div id="ContenidoOpcion" style="margin-left: 0px;">

    <?php
    $Gri_Meses = array(	"Enero" 	=> "Ccae",
        "Febrero" 	=> "Ccaf",
        "Marzo" 	=> "Ccam",
        "Abril" 	=> "Ccaa",
        "Mayo" 		=> "Ccama",
        "Junio" 	=> "Ccaj",
        "Julio" 	=> "Ccaju",
        "Agosto" 	=> "Ccaag",
        "Septiembre"=> "Ccas",
        "Octubre" 	=> "Ccao",
        "Noviembre" => "Ccan",
        "Diciembre"	=> "Ccad" );
    foreach ($Gri_Meses as $clave => $valor) {
        //echo $clave." -".$valor;
        if ($_SESSION[$valor]!="" and $_SESSION[$valor]!=0) {
            $CountDatos++;
            $suma = $suma + $_SESSION[$valor];
        } ?>
        <div class="ContOpc4C">
            <div class="ConteOpText" style="width:80px; float:left;"><b>
                    <?php echo $clave; ?></b></div>
            <div class="ConteOpText" style="float:left;">
                <input type="text" class="input_Ca" name="<?php echo $valor; ?>"
                       value="<?php echo $_SESSION[$valor]; ?>"> KWh </div>
        </div>
        <?php
    } ?>

</div>
<br>
<br>
<div class="ContOpc4C" style="height: 100px; width: 170px; background-color: #EEF4F9; padding:20px;margin: 50px;">
    <div class="ConteOpText" style="width:170px;">
        <p><b>Promedio</b></p>
        <input type="text" class="input_Ca" name="CcaP" value="<?php $Promedio = round($suma / $CountDatos); echo $Promedio; ?>">
        kWh/mes
    </div>
</div>
<div class="ContOpc4C" style="height: 100px; width: 170px; background-color: #EEF4F9; padding:20px;margin: 50px;">
    <div class="ConteOpText" style="width:170px;">
        <p><b>Total</b></p>
        <input type="text" class="input_Ca" name="" value="<?= $suma; ?>">
        kWh/mes
    </div>
</div>

<!--<hr style="box-sizing: border-box;	height: 3px;	width: 100%;	border: 1px solid #EEF4F9;" />-->
<hr class="hr" />

<h1 class="TiConfig"><img src="Img/Add-icon.png" width="15" height="15" />&nbsp;Artefactos eléctricos</h1>


<!-- //////////////// REPITE  /////////////////  -->
<?php
foreach ($_SESSION["ArtElec"] as $clave => $valor) {
    $Tipo_CcAE_Eq = array(
        "1" => "Aire Acondicionado",
        "2" => "Calefaci&oacute;n El&eacute;ctrica",
        "3" => "Otras Cargas");

    if (array_key_exists($_SESSION["ArtElec"][$clave]["CcAE_Eq"], $Tipo_CcAE_Eq)) {
        $NombreArtElec=$Tipo_CcAE_Eq[$_SESSION["ArtElec"][$clave]["CcAE_Eq"]];
    } else {
        $NombreArtElec=$_SESSION["ArtElec"][$clave]["CcAE_N"];
    }

    //echo $clave." - ".$valor." - ".$_SESSION["ArtElec"][$clave]["CcAE_Eq"];?>

    <div id="ContenidoOpcion" style="margin-left: 0px;">
        <p style="width:100%;">
            <?php echo $NombreArtElec; ?>&nbsp;<img src="Img/editar-icon.png"
                                                    width="11" height="11" /></p>

        <?php 	if ($_SESSION["ArtElec"][$clave]["CcAE_Eq"] == 2 or $_SESSION["ArtElec"][$clave]["CcAE_Eq"] == 3) {
            ?>
            <div class="ContOpc5C">
                <div class="ConteOpText">
                    <b>Potencia</b><br>
                    <input type="text" class="input_Ca" name="<?php echo $clave."-"."CcAE_P"; ?>"
                           value="<?php echo $_SESSION["ArtElec"][$clave]["CcAE_P"]; ?>"> W
                </div>
            </div>
            <?php	/*echo '<div class="ContOpc5C"><div class="ConteOpText">&nbsp;</div></div>';
            echo '<div class="ContOpc5C"><div class="ConteOpText">&nbsp;</div></div>';
            echo '<div class="ContOpc5C"><div class="ConteOpText">&nbsp;</div></div>';
            echo '<div class="ContOpc5C"><div class="ConteOpText">&nbsp;</div></div>'; */?>

            <?php
        } else {
            ?>
            <div class="ContOpc5C">
                <div class="ConteOpText">
                    <b>Tipo</b><br>
                    <?php BotonR3("CcAE_T", "Central", "Split", $clave, 'Ventana'); ?>
                </div>
            </div>

            <div class="ContOpc5C">
                <div class="ConteOpText">
                    <b>Frio/Calor</b><br>
                    <?php BotonR3("CcAE_Fc", "Si", "No", $clave); ?>
                </div>
            </div>

            <?php
            if ($_SESSION["ArtElec"][$clave]["CcAE_Fc"] == 1 or $_SESSION["ArtElec"][$clave]["CcAE_Fc"] == 0) {
                echo '<div class="ContOpc5C"><div class="ConteOpText">&nbsp;</div></div>';
                echo '<div class="ContOpc5C"><div class="ConteOpText">&nbsp;</div></div>';
            } elseif ( $_SESSION["ArtElec"][$clave]["CcAE_Fc"] == 2) {
                ?>
                <!--<div class="ContOpc5C">
                    <div class="ConteOpText">
                        <b>Calefacción a Gas</b><br>
                        <?php //BotonR3("CcAE_Cg", "Si", "No", $clave); ?>
                    </div>
                </div>

                <div class="ContOpc5C">
                    <div class="ConteOpText">
                        <b>Potencia</b><br>
                        <input type="text" class="input_Ca" name="<?php //echo $clave."-"."CcAE_P"; ?>"
                               value="<?php // echo $_SESSION["ArtElec"][$clave]["CcAE_P"]; ?>">
                        KWh
                    </div>
                </div>-->
                <?php
            }
        } ?>
    </div>
    <!--<hr style="box-sizing: border-box;	height: 3px;	width: 100%;	border: 1px solid #EEF4F9;" />-->
    <hr class="hr" />

    <?php
} ?>
<!-- //////////////// Fin  REPITE  /////////////////  -->
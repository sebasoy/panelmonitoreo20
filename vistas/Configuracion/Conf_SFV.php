<?php
include_once(__DIR__.'/MenuConfig.php');
include_once(__DIR__.'/DatosConfig.php');
include_once(__DIR__.'/Botonin.php');
?>


<div id="ContenedorOpciones">
    <div class="Tecla">
        <?php BotonI("Acsgf"); ?>
    </div>
    <div class="ContenidoOpcion">
        <div class="ConteOpTitulo">Sistema de generación Fotovoltaica</div>
        <div class="ConteOpText">Esta opción habilita el monitoreo de un sistema fotovoltaico. Con esta opción podrás
            configurar los parámetros y características del sistema.
        </div>
    </div>
</div>


<div id="ContenedorOpciones" style="margin-left: 0px;">

    <div class="ContOpc5C">
        <div class="ConteOpText">
            <b>Tipo</b><br>
            <?php BotonR("Acsgft", "String", "Microinverters", "Otro"); ?>
        </div>
    </div>

    <div class="ContOpc5C">
        <div class="ConteOpText">
            <b>Fases</b><br>
            <?php BotonR2("Acsgff", "Monofásico", "trifásico"); ?>
        </div>
    </div>

    <div class="ContOpc5C">
        <div class="ConteOpText">
            <b>Potencia nominal</b><br>
            <p>
                <input type="text" class="input_Ca" name="Acsgfpn" value="<?php echo $_SESSION["Acsgfpn"]; ?>">
                Watts
            </p>
        </div>
        <div class="ConteOpText">
            <b>Potencia del inversor</b><br>
            <p>
                <input type="text" class="input_Ca" name="Acsgfpi" value="<?php echo $_SESSION["Acsgfpi"]; ?>">
                Watts
            </p>
        </div>
    </div>

    <div class="ContOpc5C">
        <div class="ConteOpText">
            <b>Emplazamiento</b><br>
            <?php BotonR("Acsgfe", "Techo", "Piso", "BIPV"); ?>
        </div>
    </div>

    <div class="ContOpc5C">
        <div class="ConteOpText">
            <b>Orientación (Norte = 0' grados)</b><br>
            <p>
                <input type="text" class="input_Ca" name="Acsgfo" value="<?php echo $_SESSION["Acsgfo"]; ?>">
                Grados
            </p>
        </div>
        <div class="ConteOpText">
            <b>Inclinación</b><br>
            <p>
                <input type="text" class="input_Ca" name="Acsgfi" value="<?php echo $_SESSION["Acsgfi"]; ?>">
                Grados
            </p>
        </div>
    </div>

    <div class="ContOpc5C" style="width: 100%">
        <div class="ConteOpText">
            <b>Notas</b><br>
            <p>
                <textarea name="AcNotas" class="input_CN"><?php echo $_SESSION["AcNotas"]; ?></textarea>
            </p>
        </div>
    </div>
</div>

<hr class="hr" />

<h1 class="TiConfig">Generación estimada</h1>
<?php
$Gri_Meses = array(
    "Enero" 	=> "Acgee",
    "Febrero" 	=> "Acgef",
    "Marzo" 	=> "Acgem",
    "Abril" 	=> "Acgea",
    "Mayo" 		=> "Acgema",
    "Junio" 	=> "Acgej",
    "Julio" 	=> "Acgeju",
    "Agosto" 	=> "Acgeag",
    "Septiembre"=> "Acges",
    "Octubre" 	=> "Acgeo",
    "Noviembre" => "Acgen",
    "Diciembre"	=> "Acged" );
?>
<div id="wrap_ContOpc4C" style="">
    <?php foreach ($Gri_Meses as $clave => $valor) {
        //echo $clave." -".$valor;
        if ($_SESSION[$valor]!="" and $_SESSION[$valor]!=0) {
            $CountDatos++;
            $suma = $suma + $_SESSION[$valor];
        }
        //var_dump($suma)?>
        <div class="ContOpc4C">
            <div class="ConteOpText" style="width:80px; float:left;"><b>
                    <?php echo $clave; ?></b></div>
            <div class="ConteOpText" style="float:left;"><input type="text" class="input_Ca" name="<?php echo $valor; ?>"
                                                                value="<?php echo $_SESSION[$valor]; ?>">
                kWh/mes </div>
        </div>
        <?php
    } ?>

</div>

<div id="ContenedorOpciones" class="ContOpc4C" style="height: 100px; width: 170px; background-color: #EEF4F9;padding:20px;margin: 50px;">
    <div class="ConteOpText" style="width:170px;">
        <p><b>Promedio</b></p>
        <input type="text" class="input_Ca" name="Prom" value="<?php $Promedio = round($suma / $CountDatos); echo $Promedio; ?>">
        kWh/mes
    </div>
</div>

<div id="ContenedorOpciones" class="ContOpc4C" style="height: 100px; width: 170px; background-color: #EEF4F9;padding:20px;margin: 50px;">
    <div class="ConteOpText" style="width:170px;">
        <p><b>Total</b></p>
        <input type="text" class="input_Ca" name="Prom" value="<?= $suma; ?>">
        kWh/mes
    </div>
</div>

<hr class="hr" />

<!--

<h1 class="TiConfig"><img src="Img/Add-icon.png" width="15" height="15" /> Carga de documentos legales</h1>
-->
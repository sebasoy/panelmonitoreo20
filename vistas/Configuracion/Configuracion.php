<?php
require_once __DIR__."/../../modelos/db.class.php";
require_once __DIR__.'/Procesa_configuracion.php';

$query = "SELECT * FROM Usuarios WHERE USER = '". $_SESSION['USER_NAME'] ."' LIMIT 1";

$result = mysqli_query($conn, $query);

while ($fila = mysqli_fetch_assoc($result)) {
    $rol = $fila['Rol'];
}
//var_dump($rol);
?>
<?php
if ($_GET['msj'] == 'actualizado') {
    echo "<div class='wrap_msj'>
        <span class='texto_msj'>¡Informacion actualizada!</span>
        <i class='x_cerrar fas fa-times'></i>
        </div>";
}
if($_GET['C']=='Conf_Monitoreo' || $_GET['C']==''){
    $url = '?P=Configuracion&C=Conf_Monitoreo';
} elseif($_GET['C']=="Conf_Notificaciones"){
    $url = '?P=Configuracion&C=Conf_Notificaciones';
} elseif($_GET['C']=="Conf_Alertas"){
    $url = '?P=Configuracion&C=Conf_Alertas';
} elseif($_GET['C']=="Conf_Consumo"){
    $url = '?P=Configuracion&C=Conf_Consumo';
} elseif ($_GET['C']=="Conf_SFV") {
    $url = '?P=Configuracion&C=Conf_SFV';
} elseif ($_GET['C']=="Conf_Notificaciones") {
    $url = '?P=Configuracion&C=Conf_Notificaciones';
}
?>
<div id="actualiza">
    <form action="" method="post" enctype="multipart/form-data" id="id_form" name="id_form"
          onsubmit="enviarFormulario('<?= $url ?>', 'id_form', 'actualiza');<?= $_GET['C']=='Conf_Monitoreo' || $_GET['C']=='' ? 'return false;':'' ?>">
        <?php
        if ($_GET['C']=="" or !isset($_GET['C']) or $_GET['C']=="Conf_Monitoreo") {
            $SelC[1]=$Style_MC;
            include_once(__DIR__.'/Conf_Monitoreo.php'); ?>
            <input name="enviado" type="hidden" value="OK" />
            <input name="conf_monitoreo" type="hidden" value="conf_monitoreo" />
            <?php
        } elseif ($_GET['C']=="Conf_Notificaciones") {
            $SelC[2]=$Style_MC;
            include_once(__DIR__.'/Conf_Notificaciones.php');
        } elseif ($_GET['C']=="Conf_Alertas") {
            $SelC[3]=$Style_MC;
            include_once(__DIR__.'/Conf_Alertas.php');
        } elseif ($_GET['C']=="Conf_Consumo" && $rol == 1) {
            $SelC[4]=$Style_MC;
            include_once(__DIR__.'/Conf_Consumo.php'); ?>
            <input name="enviado" type="hidden" value="OK" />
            <div id="BotonGuardar">
                <div class="BotConfGuardar">
                    <button class="btn_BCG">
                        <p class="BCGP">Guardar</p>
                    </button>
                </div>
            </div>
            <?php
        } elseif ($_GET['C']=="Conf_SFV" && $rol == 1) {
            $SelC[5]=$Style_MC;
            include_once(__DIR__.'/Conf_SFV.php'); ?>
            <input name="enviado" type="hidden" value="OK" />
            <div id="BotonGuardar">
                <div class="BotConfGuardar">
                    <button class="btn_BCG">
                        <p class="BCGP">Guardar</p>
                    </button>
                </div>
            </div>
            <?php
        }else{
            include_once(__DIR__.'/Conf_Monitoreo.php'); ?>
        <?php } ?>
    </form>
</div>
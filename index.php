<?php
session_start();
CONST SEGUNDOS_PARA_DESLOGUEO = 1800; // 1800 seg -> 30 MINUTOS
if ((isset($_SESSION['LAST_ACTIVITY']) && time() - $_SESSION['LAST_ACTIVITY'] < SEGUNDOS_PARA_DESLOGUEO) && $_GET['P'] != "Logout") {
    $_SESSION['LAST_ACTIVITY'] = time();
} else {
    $_GET['P'] = "Logout";
    // last request was more than 30 minutes ago
    session_unset();     // unset $_SESSION variable for the run-time
    session_destroy();   // destroy session data in storage
    $_SESSION = [];     // este e el posta! te vacia la CECION!
    $_SESSION['LAST_ACTIVITY'] = time();
}



include_once(__DIR__.'/vistas/cabecera.php');
include_once(__DIR__.'/vistas/cuerpo.php');

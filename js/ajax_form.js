function nuevoAjax() {
    var xmlhttp = false;
    try {
        xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
        try {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (E) {
            xmlhttp = false;
        }
    }

    if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
        xmlhttp = new XMLHttpRequest();
    }
    return xmlhttp;
}

function enviarFormulario(url, formid, divrespuesta) {
    var Formulario = document.getElementById(formid);
    var longitudFormulario = Formulario.elements.length;
    var cadenaFormulario = "";
    var sepCampos;
    sepCampos = "";
    for (var i = 0; i <= Formulario.elements.length - 1; i++) {
        cadenaFormulario += sepCampos + Formulario.elements[i].name + '=' + encodeURI(Formulario.elements[i].value);
        sepCampos = "&";
    }
    //$("#respuesta").
    //alert(cadenaFormulario);
    peticion = nuevoAjax();
    peticion.open("POST", url, true);
    peticion.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=ISO-8859-1');
    peticion.send(cadenaFormulario);
    peticion.onreadystatechange = function () {
        //console.log(peticion.readyState);
        if (peticion.readyState == 4 || peticion.readyState == 2 && (peticion.status == 200 || window.location.href.indexOf("http") == -1)) {
            //$(".ItMenuConfig").click();
        }
    }
}

//funcion esconder mensaje de exito o error.
function esconder() {
    $('.x_cerrar').parent().fadeOut(2000);
    return new Promise(function(resolve, reject) {
        setTimeout(function() {
            resolve();
        }, 2000);
    })
}
$(document).ready(function(){
    //click en cerrar esconde el mensaje y luego lo elimina
    $("body").on('click','.x_cerrar',function(){
        esconder()
            .then(function () {
                $('.x_cerrar').parent().remove();
            })
            .catch(function() {
               alert('Error');
            });
    });
    //boton expandir del grafico
    $("body").on('click','#expandir_grafico_monitoreo',function(){
        $("#esconder_graficos, .fa-plus-circle, .fa-minus-circle").toggle("slow");
        drawVisualization();
        $("html, body").animate({ scrollTop: $(document).height() }, 2000);
    });
    setTimeout(function () {
        $(".x_cerrar").click();
    },1000);
    //$('html, body').scrollTop($("#chart_div").offset().top);
});


function Mu_Oc(id) {
    //	var elem = document.getElementById("pantalla_completa");
    //	elem.style.height = document.getElementById("contenedor").scrollHeight + "px";
    //	var elem = document.getElementById("centrado_frente");
    //	elem.style.top = "50%";
    $("#"+id).toggle("slow");
   /* if (document.getElementById) {
        var el = document.getElementById(id);
        el.style.display = (el.style.display == 'block') ? 'none' : 'block';
    }*/
}